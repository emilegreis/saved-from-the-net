# saved from the net

Some scripts I make to download/archive stuff from the web (wget, bs4, selenium, etc.).

- chatwerkers-gif-sammlungen-alphabete: Download helper for the beautiful gifs glyphs/fonts from <https://sites.google.com/site/weltdergifs02/animierte-gifs-29>.
- Osanaetoki.Bankokubanashi: Download the digitized pages of the 1861 book by writer Kanagaki Robun and artist Utagawa Yoshitora from the [Waseda University website](https://archive.wul.waseda.ac.jp/kosho/bunko11/bunko11_a0380/bunko11_a0380_0002/bunko11_a0380_0002.html).
- r-e-i-n-f-u-r-t: Download PDFs from David Reinfurt courses at Princeton University (<https://www.t-y-p-o-g-r-a-p-h-y.org/>, <https://www.g-e-s-t-a-l-t.org/>, <https://www.i-n-t-e-r-f-a-c-e.org/>).
- Visible.Language.Journal: Download and rename PDFs of the [Visible Language Journal](https://journals.uc.edu/index.php/vl/index).
