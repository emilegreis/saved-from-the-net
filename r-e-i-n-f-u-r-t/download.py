#!/usr/bin/env python

import os
import subprocess 
import requests
from bs4 import BeautifulSoup

sites = [
'https://www.t-y-p-o-g-r-a-p-h-y.org/',
'https://www.i-n-t-e-r-f-a-c-e.org/',
'https://www.g-e-s-t-a-l-t.org/'
]

for site in sites:
    course = site.replace('https://www.','').replace('/','')
    print('----')
    print(site)
    print(course)

    if not os.path.exists('data/'+course): 
        os.makedirs('data/'+course)
    
    r = requests.get(site + 'read/')
    soup = BeautifulSoup(r.text, 'html.parser')
    links = soup.find(id='main').find_all('a')
    links.pop(0)
    links.pop(0)
    for link in links:
        url = site+link['href']
        print(url)
        subprocess.run(['wget', '-4', '-t', '1', '-P', 'data/'+course, url])
