#!/usr/bin/env python

import sys
import os
import requests
from bs4 import BeautifulSoup

# ranges
number  = [48,57]
upper   = [65,90]
lower   = [97,122]

# args
name = 'agace'
url = 'https://sites.google.com/site/weltdergifs02/animierte-gifs-29'
ranges = [upper, lower, number] # change according to web page images sequence or use custom number

decimals = []

for i in ranges:
    if (type(i)) == list:
        for j in range(i[0], i[1]+1):
            decimals.append(j)
    if (type(i)) == int: # int
        for j in i:
            decimals.append(ord(j))

print(decimals)

r = requests.get(url)
soup = BeautifulSoup(r.text, 'html.parser')

content = soup.find_all(class_="sites-tile-name-content-1")[0]
images = content.find_all('img')

os.mkdir('data/' + name)

for i in range(len(images)): 
    print(i, images[i]['src'], decimals[i])

    r = requests.get(images[i]['src'], timeout=10)
    if r.status_code == 200:
        with open('data/' + name + '/' + str(decimals[i]) + '.gif', 'wb') as f:
            f.write(r.content)
            f.close()
    else:
        print('ERROR:', i, images[i], decimals[i])
