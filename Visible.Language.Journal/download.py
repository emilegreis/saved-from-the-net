#!/usr/bin/env python

import sys
import re
import subprocess 
import unicodedata
import os
import json
import requests
from bs4 import BeautifulSoup
from datetime import datetime

journal = []

def download_issue(url):
    print()
    print('-----')
    issue = {}
    issue['link'] = url
    # Open issue page
    try:
        requests.get(issue['link'])
    except:
        print('no soup')
    else:
        r = requests.get(issue['link'])
        soup = BeautifulSoup(r.text, 'html.parser')
    # Get issue info
    try:
        soup.find('h1').text.strip().split(':')[0]
    except:
        print('no title')
    else:
        title = soup.find('h1').text.strip().split(':')[0]
        issue['vol'] = title.split(' ')[1]
        issue['no'] = title.split(' ')[3]
        issue['year'] = title.split('(')[1].split(')')[0]
    try:
        soup.find(class_='galleys_links').find('a', class_='pdf')
    except:
        print('no pdf link')
    else:
        if 'restricted' not in soup.find(class_='galleys_links').find('a', class_='pdf')['class']:
            issue['pdf'] = soup.find(class_='galleys_links').find('a', class_='pdf')['href']
        else:
            print('restricted pdf')
    print(issue)
    journal.append(issue)
    # Download issue
    if issue['year'] and issue['vol'] and issue['no']:
        issuetitle = issue['year'] + '_Vol.' + issue['vol'] + '_No.' + issue['no']
    else:
        issuetitle = datetime.now()

    if 'pdf' in issue:
        if os.path.exists('data/'+issuetitle+'.pdf'):
            print(issuetitle + '.pdf already exists')
        else:
            downloadlink = issue['pdf'].replace('view', 'download')
            subprocess.run(['wget', '-4', '-t', '1', '-O', 'data/'+issuetitle+'/'+issuetitle+'.pdf', downloadlink])
            print(downloadlink)
    else:
        print('restricted pdf')



# Iterate pages
for p in range(1,9,1):
    print(p,p,p,p,p,p)
    # Open page
    r = requests.get('https://journals.uc.edu/index.php/vl/issue/archive/'+str(p))
    soup = BeautifulSoup(r.text, 'html.parser')
    # Iterates issues
    issues = soup.find_all(class_='obj_issue_summary')
    for i in issues:
        try:
            i.find(class_='title')['href']
        except:
            print('no link')
        else:
            download_issue(i.find(class_='title')['href'])



#sortedd = sorted(journal, key=lambda x: (x.get('year', '0'), x.get('vol', '0'), x.get('no', '0')), reverse=True) 
#pprint.pprint(sortedd)
#with open('articles.json', 'w', encoding='utf-8') as f: 
#    json.dump(sortedd, f, ensure_ascii=False, indent=4)



# tests
#download_issue('https://journals.uc.edu/index.php/vl/issue/view/593')
#download_issue('https://journals.uc.edu/index.php/vl/issue/view/460')
#download_issue('https://journals.uc.edu/index.php/vl/issue/view/295')
#download_issue('https://journals.uc.edu/index.php/vl/issue/view/296')
