- [2024 - Vol. 58 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/593)
  + [An Exploratory Study Evaluating the Influence of Taller Stripe Patterns on Reading Comfort Using Ranking Tests, Reading Tests, EEG’s, and Eye Tracking](https://journals.uc.edu/index.php/vl/article/view/8385) - Maarten Renckens
  + [Snapshots of Text on Instagram](https://journals.uc.edu/index.php/vl/article/view/8386) - Katharina Sand
- [2023 - Vol. 57 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/584)
  + [The Pictorial Trapezoid](https://journals.uc.edu/index.php/vl/article/view/8284) - Matthew Peterson, Ashley L. Anderson, Kayla Rondinelli, Helen Armstrong
  + [Online Archive Improvement Study](https://journals.uc.edu/index.php/vl/article/view/8285) - D.J. Trischler
  + [New Model](https://journals.uc.edu/index.php/vl/article/view/8286) - Mike Zender
- [2023 - Vol. 57 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/576)
  + [The perception of qualities in typefaces: a data review](https://journals.uc.edu/index.php/vl/article/view/6381) - Andrea Piovesan, Michele Sinico, Luciano Perondi
  + [Dirty Concrete Poetry and White Space](https://journals.uc.edu/index.php/vl/article/view/6564) - Steven McCarthy
  + [THE TACIT EXHIBITION](https://journals.uc.edu/index.php/vl/article/view/7532) - Halldór Björn Halldórsson
  + [Dirty Concrete Poetry and White Space](https://journals.uc.edu/index.php/vl/article/view/8183) - Steven McCarthy
  + [The Perception of Qualities in Typeface](https://journals.uc.edu/index.php/vl/article/view/8184) - Andrea Piovesan, Michele Sinico, Luciano Perondi
  + [The Tacit Exhibition](https://journals.uc.edu/index.php/vl/article/view/8185) - Halldór Björn Halldórsson
  + [A Book to Think With](https://journals.uc.edu/index.php/vl/article/view/8187) - Sharon Helmer Poggenpohl
- [2023 - Vol. 57 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/524)
  + [Introduction](https://journals.uc.edu/index.php/vl/article/view/7322) - Myra Thiessen, Daphne Flynn, Leah Heiss, Rowan Page, Nyein Aung, Indae Hwang
  + [The Future is Participatory](https://journals.uc.edu/index.php/vl/article/view/7318) - Myra Thiessen, Leah Heiss, Troy McGee, Gene Bawden
  + [The Human and Machine, 2022-23](https://journals.uc.edu/index.php/vl/article/view/7319) - Wendy Ellerton
  + [Show Me What You Mean](https://journals.uc.edu/index.php/vl/article/view/7320) - Darren Taljaard, Myra Thiessen
- [2022 - Vol. 56 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/488)
  + [The Inclusive Design of  pictograms and easy to understand for people with intellectual disabilities](https://journals.uc.edu/index.php/vl/article/view/5976) - Mao Kudo
  + [Effect of Typeface Complexity on Automatic Whole-Word Reading Processes](https://journals.uc.edu/index.php/vl/article/view/6393) - Myra Thiessen, Hannah Keage, Indae Hwang, Jack Astley, Sofie Beier
  + [Design Features of Learning Apps for Mobile Gamification](https://journals.uc.edu/index.php/vl/article/view/6394) - Caroline Tjung, Simone Taffe, Simon Jackson, Emily Wright
  + [Graphic Design of Pictograms Focusing on the Comprehension of People with Intellectual Disabilities – The Next Step in Standardization](https://journals.uc.edu/index.php/vl/article/view/6395) - Mao Kudo
- [2022 - Vol. 56 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/471)
  + [Academic Research Into Typographic Design at the Beginning of a New Era](https://journals.uc.edu/index.php/vl/article/view/6057) - Ann Bessemans
  + [Persistent Failure and Occasional Success](https://journals.uc.edu/index.php/vl/article/view/6060) - Matthew Peterson
  + [Graphic Design in Public Health Research](https://journals.uc.edu/index.php/vl/article/view/6061) - Michael Schmidt, Taghrid Asfar, Wasim Maziak
  + [Studying Typography's Capacity to Improve Reading](https://journals.uc.edu/index.php/vl/article/view/6068) - Reneé Seward
- [2022 - Vol. 56 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/291)
  + [Early Writing](https://journals.uc.edu/index.php/vl/article/view/4934) - Karenleigh A.  Overmann
  + [Brainstorm](https://journals.uc.edu/index.php/vl/article/view/4935) - Simone Taffe, Leon Sterling, Sonja Pedell
  + [Symbols](https://journals.uc.edu/index.php/vl/article/view/4936) - Mike Zender
- [2021 - Vol. 55 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/264)
  + [Introduction: The makers and the made](https://journals.uc.edu/index.php/vl/article/view/4678) - Mike Zender
  + [Beyond Judging Books by Their Covers: "Reflections on Interrogating Cultural Anthropology Text Covers"](https://journals.uc.edu/index.php/vl/article/view/4669) - Naomi S. Baron
  + [Exploring the Boundaries Between Visual Anthropology and Visual Communication Design Research](https://journals.uc.edu/index.php/vl/article/view/4670) - Ann Bessemans, María  Pérez Mena
  + [A commentary on: Reappropriation of Gendered Irish Sign Language in One Family](https://journals.uc.edu/index.php/vl/article/view/4671) - Mary Dyson
  + [Empathic Projections: Visual Anthropology Design and Acknowledgement](https://journals.uc.edu/index.php/vl/article/view/4672) - Micheal J. Golec
  + [Re reading the Borderland Imaginary from 2021](https://journals.uc.edu/index.php/vl/article/view/4673) - Dori Griffin
  + [Echoing the Call for Multimodal Representation](https://journals.uc.edu/index.php/vl/article/view/4674) - Michael Renner
  + [Drawing to Tell Versus Drawing to Intrigue?](https://journals.uc.edu/index.php/vl/article/view/4676) - Michael Renner
  + [Developing Accurate Visual Conventions?](https://journals.uc.edu/index.php/vl/article/view/4677) - Karel van der Waarde
- [2021 - Vol. 55 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/262)
  + [Visual prosody supports reading aloud expressively for deaf readers](https://journals.uc.edu/index.php/vl/article/view/4603) - Maarten Renckens, Leo De Raeve, Erik Nuyts, María Pérez Mena, Ann Bessemans
  + [A preliminary study exploring the relation between visual prosody and the prosodic components in sign language](https://journals.uc.edu/index.php/vl/article/view/4604) - Maaten Renckens, Leo De Raeve, Erik Nuyts, María Pérez Mena, Ann Beesemans
  + [Co-designing to Improve Practice in Treating Urinary Tract Infections: a case study of reducing inappropriate antibiotic treatment](https://journals.uc.edu/index.php/vl/article/view/4605) - Guillermina Noël, Daren Pasay, Denise Campbell-Scherer, Lynora Saxinger
- [2020 - Vol. 54 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/261)
  + [The Characteristics of Text and Display Sizes in 16th Century Flemish Roman Type](https://journals.uc.edu/index.php/vl/article/view/4609) - Krassen Krestev
  + [Consequently Positioning the Rhythm in Type Based on the Letters’ Longest Continuous Black Mass](https://journals.uc.edu/index.php/vl/article/view/4610) - Maarten Renckens
  + [The Influence of Macrotypography on the Comprehensibility of Texts in Easy-to- Read Language](https://journals.uc.edu/index.php/vl/article/view/4611) - Sabine Sieghart
- [2020 - Vol. 54 - No. 1-2](https://journals.uc.edu/index.php/vl/issue/view/265)
  + [Special Issue: Introduction](https://journals.uc.edu/index.php/vl/article/view/4628) - Jeanne-Louise Moys
  + [Readers' experiences of Braille in an evolving technological world](https://journals.uc.edu/index.php/vl/article/view/4629) - Laura Marshall, Jeanne-Louise Moys
  + [Linguistic and cultural design features of the manual syllabary in Japan](https://journals.uc.edu/index.php/vl/article/view/4612) - Angela M Nonaka, Jean Ann, Keiko Sagara
  + [A Latin-script typeface, based on special education teachers' opinions, to use in literacy education of individuals with autism](https://journals.uc.edu/index.php/vl/article/view/4626) - Efecan Serin, Aprigio Luis Moreira Morgado, Ricardo Santos
  + [Anticipating Gaze-Based HCI Applications with the Tech Receptivity Interval: Eye Tracking as Input](https://journals.uc.edu/index.php/vl/article/view/4627) - Matthew Peterson, Brad Tober, Deborah Littlejohn, Mac Hill
  + [Enhancing Bowel Cancer Surgery Recovery through Information Design: The impact of combining design and cognitive principles with user-centered research methods, on patient understanding of surgery recovery information](https://journals.uc.edu/index.php/vl/article/view/4630) - Maria do Santos Lonsdale, Stephanie Sciberras, Hyejin Ha, Stephen J Chapman
  + [Impact of Kinetic Typography on Readers' Attention](https://journals.uc.edu/index.php/vl/article/view/4632) - Milda Kuraitytė, Ann Bessemans, Erik Nuyts
  + [Computers and Challenges of Writing in Persian: Explorations at the Intersection of Culture and Technology](https://journals.uc.edu/index.php/vl/article/view/4631) - Behrooz Parhami
- [2019 - Vol. 53 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/270)
  + [E-Inclusion: Defining Basic Image Properties for Illustrated Stimuli in Aphasia Treatment](https://journals.uc.edu/index.php/vl/article/view/4652) - Claire Reymond, Christine Müller, Indre Grumbinaite
  + [Visual Prosody Supports Reading Aloud Expressively](https://journals.uc.edu/index.php/vl/article/view/4653) - Ann Bessemans, Maarten Renckens, Kevin Bormans, Erik Nuyts, Kevin Larson
  + [The  effect of age and font on reading ability](https://journals.uc.edu/index.php/vl/article/view/4654) - Sofie Beier, Chiron A T Oderkerk
  + [Letterform Legibility and Visual Perception: a speculation](https://journals.uc.edu/index.php/vl/article/view/4655) - Mike Zender
- [2019 - Vol. 53 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/269)
  + [For Visual Attention, are there any Tendencies in Form Interpretation?](https://journals.uc.edu/index.php/vl/article/view/4650) - Jinsook Kim, Michael H Fritsch
  + [Visualizing the terror threat](https://journals.uc.edu/index.php/vl/article/view/4645) - Maria dos Santos Lonsdale, David J Lonsdale, Matthew Baxter, Ryan Graham, Aya Kanafani, Anqi Li, Chunxinzi Peng
  + [Legibility of Pharmaceutical Pictograms](https://journals.uc.edu/index.php/vl/article/view/4646) - Pia Pedersen
  + [Dynamic Visual Identities](https://journals.uc.edu/index.php/vl/article/view/4649) - Tiago Martins, João M Cunha, João Bicker, Penousal Machado
- [2019 - Vol. 53 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/267)
  + [Histories of Visual Communication Design](https://journals.uc.edu/index.php/vl/article/view/4620) - Dori Griffin
  + [The Implications of Media](https://journals.uc.edu/index.php/vl/article/view/4622) - Hala Auji
  + [Ismar David’s Quest for Original Hebrew Typographic Signs](https://journals.uc.edu/index.php/vl/article/view/4623) - Shani Avni
  + [Mana mātātuhi](https://journals.uc.edu/index.php/vl/article/view/4624) - Dr. Johnson Witehira
  + [Lower Case in the Flatlands](https://journals.uc.edu/index.php/vl/article/view/4625) - Trond Klevgaard
- [2018 - Vol. 52 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/268)
  + [Note from the editor](https://journals.uc.edu/index.php/vl/article/view/4643) - Maria dos Santos Lonsdale
  + [Informing personal branding through self-assessed handwriting analysis: proposal of a supportive online platform](https://journals.uc.edu/index.php/vl/article/view/4640) - Bakalka Laura, Catarina Lelis
  + [Informative and decorative pictures in health and safety posters for children](https://journals.uc.edu/index.php/vl/article/view/4639) - Sara Klohn, Alison Black
  + [Two-dimensional vs three-dimensional guide maps: which work best for museum visitors?](https://journals.uc.edu/index.php/vl/article/view/4642) - Andrew McIlwraith
  + [eInk versus paper: exploring the effects of medium and typographic quality on recall and reading speed](https://journals.uc.edu/index.php/vl/article/view/4634) - Jeanne-Louise Moys, Loveland Peter, Mary C Dyson
  + [Exploring illustration styles for materials used in visual resources for people with aphasia](https://journals.uc.edu/index.php/vl/article/view/4635) - Jeanne-Louise Moys, Carmen Martínez-Freile, Rachel McCrindle, Lotte Meteyard, Holly Robson, Luke Kendrick, Maitreyee Wairagkar
  + [Garment label design and companion information to communicate fashion sustainability issues to young consumers](https://journals.uc.edu/index.php/vl/article/view/4636) - Perez Ana, Maria dos Santos Lonsdale
  + [Designing on Ntaria Country: telling stories with new tools](https://journals.uc.edu/index.php/vl/article/view/4633) - Nicola St John
  + [How can the principles and practices of information design help us produce useful live art documentation?](https://journals.uc.edu/index.php/vl/article/view/4638) - Traina Rosanna
  + [Spaces that Speak: Exploring Creative Opportunities in the Communication Design of Conversational Spaces](https://journals.uc.edu/index.php/vl/article/view/4641) - Lysbakken Nina
- [2018 - Vol. 52 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/466)
  + [Counting But Losing Count: the legacy of Otto Neurath's Isotype charts](https://journals.uc.edu/index.php/vl/article/view/5954) - Pino Trogu
  + [Same But Different: A framework for understanding conceptions of research in communication design practice and academia](https://journals.uc.edu/index.php/vl/article/view/5955) - Emma Fisher, Nicolette Lee, Scott  Thompson-Whiteside
  + [Reviewing Open-access Icons for Emergency: a case study testing meaning performance in Guemil](https://journals.uc.edu/index.php/vl/article/view/5956) - Rodrigo Ramírez
  + [Using Design Research for a Better Understanding of Complex Problems: a study of two homes for the elderly](https://journals.uc.edu/index.php/vl/article/view/5957) - Brian Switzer
- [2017 - Vol. 51 - No. 3-1](https://journals.uc.edu/index.php/vl/issue/view/465)
  + [Practice-led Iconic Research: Towards a Research Methodology for Visual Communication](https://journals.uc.edu/index.php/vl/article/view/5946) - Michael Renner
  + [The Practice of Practice-led Iconic Research](https://journals.uc.edu/index.php/vl/article/view/5947) - Arno Schubbach
  + [The Dynamism of Hangeul's Vertical Strokes and the Flow of Its Lines of Writing](https://journals.uc.edu/index.php/vl/article/view/5948) - Jinsu Ahn
  + [The Image as Unstable Constellation: Rethinking Darwin's Diagram from the Perspective of Practice-led Iconic Research](https://journals.uc.edu/index.php/vl/article/view/5949) - Paloma López Grüninger
  + [Premises for Interaction between Images](https://journals.uc.edu/index.php/vl/article/view/5950) - Claire Reymond
  + [Making Things Visible: Visual Strategies for the Representation of Objects in Collections](https://journals.uc.edu/index.php/vl/article/view/5951) - Michael Hübner
  + [Documentary Image Sequences](https://journals.uc.edu/index.php/vl/article/view/5952) - Susanne Käser
  + [Identifying Design Processes in Photography by Analyzing Photographic Strategies in the Documentation of Public Places: "It's hard to be down when you're up."](https://journals.uc.edu/index.php/vl/article/view/5953) - Helga Aichmaier
- [2017 - Vol. 51 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/464)
  + [DrawIt: a user-drawn design research method for symbol design](https://journals.uc.edu/index.php/vl/article/view/5941) - Mike Zender
  + [Bespoke Wayshowing in Hospitals](https://journals.uc.edu/index.php/vl/article/view/5942) - Thomas Ockerse, Per Mollerup
  + [The Written Adornment: the many relations of text and image in Classic Maya visual culture](https://journals.uc.edu/index.php/vl/article/view/5943) - Daniel Salazar Lama, Rogelio Valencia Rivera
  + [Metonymic and Metaphoric Series in the Codex Borgia. Plates 33-34.](https://journals.uc.edu/index.php/vl/article/view/5944) - Angélica Baena Ramírez
  + [Design by Consensus: A New Method for Designing Effective Pictograms](https://journals.uc.edu/index.php/vl/article/view/5945) - Mike Zender, Alisa Strauss
- [2017 - Vol. 51 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/463)
  + [Behind Isotype Charts: The Design of Number-Fact Pictures](https://journals.uc.edu/index.php/vl/article/view/5935) - Pia Pedersen
  + [Signs of resistance:  Iconography and semasiography in Otomi architectural decoration and manuscripts of the early  colonial period](https://journals.uc.edu/index.php/vl/article/view/5936) - David Charles Wright-Carr
  + [Legibility Implications of  Embellished Display Typefaces](https://journals.uc.edu/index.php/vl/article/view/5937) - Sofie Beier, Katrine Sand, Randi Starrfelt
  + [Illuminography:  a survey of the  pictorial language  of Hong Kong' s neon signs](https://journals.uc.edu/index.php/vl/article/view/5938) - Brian Sze-Hang Kwok, Anneke Coppoolse
  + [Tz' ihb' write/paint'. Multimodality in Maya glyphic texts](https://journals.uc.edu/index.php/vl/article/view/5939) - Agnieszka Hamann
  + [What exactly is the difference  between a text and a display typeface?](https://journals.uc.edu/index.php/vl/article/view/5940) - Chuck Bigelow, Chuck Bigelow, Mary Dyson, Maria dos dos Santos Lonsdale, Kevin Larson
- [2016 - Vol. 50 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/462)
  + [The Role of Visible Language in Building and Critiquing a Canon of Graphic Design History](https://journals.uc.edu/index.php/vl/article/view/5932) - Dori Griffin
  + [Graphic Designers' Sense and  Knowledge of the User:  Is thinking differently the groundwork  for acting differently?](https://journals.uc.edu/index.php/vl/article/view/5933) - Dr. Nicole Wragg, Dr. Carolyn Barnes
  + [How Humans Process  Visual Information:  A focused primer for  designing information](https://journals.uc.edu/index.php/vl/article/view/5934) - Lou Tetlan, Douglas Marschalek
- [2016 - Vol. 50 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/461)
  + [The Xerox Alto Font Design System](https://journals.uc.edu/index.php/vl/article/view/5920) - Patrick Baudelaire
  + [The Digital Typefoundry](https://journals.uc.edu/index.php/vl/article/view/5921) - Matthew Carter
  + [Communication of Mathematics with TEX](https://journals.uc.edu/index.php/vl/article/view/5922) - Barbara  Beeton, Richard  Palais
  + [Letterform Research: an academic orphan](https://journals.uc.edu/index.php/vl/article/view/5923) - Sofie  Beier
  + [Commercial at @](https://journals.uc.edu/index.php/vl/article/view/5924) - James  Mosley
  + [Orthographic Processing and Reading](https://journals.uc.edu/index.php/vl/article/view/5925) - Jonathan  Grainger
  + [Reading Digital with Low Vision](https://journals.uc.edu/index.php/vl/article/view/5926) - Gordon E. Legge
  + [Exploring the relationship between language and design: a study of Hong Kong newspapers](https://journals.uc.edu/index.php/vl/article/view/5927) - Ryan Lee, Jeanne-Louise Moys
  + [Remembrances of eminent contributors to Visible Language's first 50 years...](https://journals.uc.edu/index.php/vl/article/view/5928) - 
  + [Top 50 typography books of the last 50 years](https://journals.uc.edu/index.php/vl/article/view/5929) - 
  + [Book history scholarship: creation, transmission of knowledge and archives](https://journals.uc.edu/index.php/vl/article/view/5930) - Danné Ojeda, Mathieu Lommen
  + [Typography Education: Eco-System and Excellence](https://journals.uc.edu/index.php/vl/article/view/5931) - Reneé Seward, Emily Verba Fischer
- [2016 - Vol. 50 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/460)
  + [Normal science" and the changing practices of design and design education](https://journals.uc.edu/index.php/vl/article/view/5913) - Meredith Davis
  + [Typographic features of text and their contribution to the legibility of academic reading materials: An empirical study.](https://journals.uc.edu/index.php/vl/article/view/5914) - Dr Maria dos Santos Lonsdale
  + [Design Research Pioneer Josef Albers: a case for design research](https://journals.uc.edu/index.php/vl/article/view/5915) - Mike Zender
  + [Design Journals: Context, Serendipity, and Value](https://journals.uc.edu/index.php/vl/article/view/5916) - Sharon Helmer Poggenpohl
  + [Calculating Line Length: an arithmetic approach](https://journals.uc.edu/index.php/vl/article/view/5917) - Ernesto Peña
  + [Recognizing appropriate representation of indigenous knowledge in design practice](https://journals.uc.edu/index.php/vl/article/view/5918) - Meghan Kelly (PhD), Russell Kennedy (PhD, FRSA. FIDA)
  + [Pictograms: Can they help patients recall medication safety instructions?](https://journals.uc.edu/index.php/vl/article/view/5919) - Louis  Del Re, B.Sc, Dr. Régis Vaillancourt, Pharm D, B, Pharm, Dr. Gilda Villarreal, PhD, MHA, Dr. Annie Pouliot, PhD
- [2015 - Vol. 49 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/459)
  + [Meta! Meta! Meta! A Speculative Design Brief for the Digital Humanities](https://journals.uc.edu/index.php/vl/article/view/5904) - Anne  Burdick
  + [Clues. Anomalies. Understanding. Detecting underlying assumptions and expected practices in the Digital Humanities through the AIME project](https://journals.uc.edu/index.php/vl/article/view/5905) - Donato Ricci, Robin de Mourat, Christophe Leclercq, Bruno Latour
  + [Writing Images and the Cinematic Humanities](https://journals.uc.edu/index.php/vl/article/view/5906) - Holly Willis
  + [Beyond the Map: Unpacking Critical Cartography in the Digital Humanities](https://journals.uc.edu/index.php/vl/article/view/5907) - Tania  Allen, Sara Queen
  + [The Idea and Image of Historical Time: Interactions between Design and Digital Humanities](https://journals.uc.edu/index.php/vl/article/view/5908) - Stephen Boyd Davis, Florian Kräutli
  + [Critical Interfaces and Digital Making](https://journals.uc.edu/index.php/vl/article/view/5909) - Steve Anderson
  + [Making Culture: Locating the Digital Humanities in India](https://journals.uc.edu/index.php/vl/article/view/5910) - Padmini Ray Murray, Chris Hand
  + [Prototyping the Past](https://journals.uc.edu/index.php/vl/article/view/5911) - Jentery Sayers
  + [Book Art: a Critical Remix of The Electric Information Age Book](https://journals.uc.edu/index.php/vl/article/view/5912) - Steven McCarthy
- [2015 - Vol. 49 - No. 1-2](https://journals.uc.edu/index.php/vl/issue/view/458)
  + [Targeted Communication to Reduce Antibiotic Prescription](https://journals.uc.edu/index.php/vl/article/view/5893) - Regina Hanke
  + [Connotative Localization of an HIV Prevention  Image to Promote Safer Sex Practices in Ghana](https://journals.uc.edu/index.php/vl/article/view/5894) - Audrey Bennett
  + [Designing and Evaluating a Health Program in Africa: Hygiene Matters](https://journals.uc.edu/index.php/vl/article/view/5895) - Mike Zender, David K. Plate
  + [Designing a Visual Tool to Interview  People with Communication Disabilities: a user-centered approach](https://journals.uc.edu/index.php/vl/article/view/5896) - Guillermina Noël
  + [Using Icons to Overcome Communication Barriers During  Emergencies:  a case study of the Show Me interactive tools](https://journals.uc.edu/index.php/vl/article/view/5897) - Amina Patton, Morgan Griffin, Ana Tellez, Mary Ann Petti, Xanthi Scrimgeour
  + [Developing a Design Brief for a Virtual Hospice  Using Design Tools and Methods: a preliminary exploration](https://journals.uc.edu/index.php/vl/article/view/5898) - Andrea Taylor, Tara French, Jeni Lennox, Dr. Jeremy Keen
  + [Trans-disciplinary Partnerships in  IT Health Software Development: the benefits to learning](https://journals.uc.edu/index.php/vl/article/view/5899) - Sarah Lowe, Tami H. Wyatt, Xueping Li, Susan Fancher
  + [Co-designing for Healthcare: visual designers as researchers and facilitators](https://journals.uc.edu/index.php/vl/article/view/5900) - Pamela Napier, Terri Wada
  + [Design to Improve the Health  Education Experience:  using participatory design methods in hospitals with clinicians and patients](https://journals.uc.edu/index.php/vl/article/view/5901) - Belinda Paulovich
  + [Design and Language Impact on  Study Volunteerism in Medical Research:  learnings from a controlled study of recruitment letters](https://journals.uc.edu/index.php/vl/article/view/5902) - Helen Sanematsu, Brenda Hudson, Amanda Nyhuis, Paul Dexter, Siu Hui
  + [Brainy Type:  a look at how the brain processes typographic information](https://journals.uc.edu/index.php/vl/article/view/5903) - Dr Myra Thiessen, Dr Mark Kohler, Dr Owen Churches, Scott Coussens, Dr Hannah Keage
- [2014 - Vol. 48 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/457)
  + [Visual Improvisation: Cognition, Materiality, and Postlinguistic Visual Poetry](https://journals.uc.edu/index.php/vl/article/view/5886) - Mike Borkent
  + [Typographic Features of Text: Outcomes from Research and Practice](https://journals.uc.edu/index.php/vl/article/view/5887) - 
  + [A Statistical Approach for Visualizing the Quality of Multi-Hospital Data](https://journals.uc.edu/index.php/vl/article/view/5888) - 
  + [Linking Design Principles  with Educational Research Theories  to Teach Sound to Symbol Reading Correspondence with Multisensory Type](https://journals.uc.edu/index.php/vl/article/view/5889) - 
  + [Book Review: Design for information, an introduction to the histories, theories, and best practices behind effective information visualizations.](https://journals.uc.edu/index.php/vl/article/view/5890) - Isabel Meirelles
  + [Book Review: Isotype: Design and contexts 1925–1971](https://journals.uc.edu/index.php/vl/article/view/5891) - 
  + [Book Review: The Most Important Design Books Most Designers Have Never Read:](https://journals.uc.edu/index.php/vl/article/view/5892) - Mike Zender
- [2014 - Vol. 48 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/456)
  + [Map Design for Complex Architecture:A User Study of Maps & Wayfinding](https://journals.uc.edu/index.php/vl/article/view/5881) - Karen Cheng, Sarah Pérez-Kriz
  + [The Environment is (Still)Not in the Head:Harry Heft & Contemporary Methodological Approaches to Navigation and Wayfinding](https://journals.uc.edu/index.php/vl/article/view/5882) - Ashley Walton
  + [Learning Design Thinking byDesigning Learning Experiences:A Case Study in the Development of Strategic Thinking Skills through the Design ofInteractive Museum Exhibitions](https://journals.uc.edu/index.php/vl/article/view/5883) - Lisa Fontaine
  + [On the Wall:Designers as Agents for Change in Environmental Communication](https://journals.uc.edu/index.php/vl/article/view/5884) - Patricia Cué
  + [Rebuilding Perceptions:Using Experiential Graphic Design to Reconnect Neighborhoods to the Greater City Population](https://journals.uc.edu/index.php/vl/article/view/5885) - Andrew T. Schwanbeck
- [2014 - Vol. 48 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/455)
  + [Slide Presentations, Seriously](https://journals.uc.edu/index.php/vl/article/view/5877) - Per Mollerup
  + [The Integration of Text and Image in Media and Its Impact on Reader Interest](https://journals.uc.edu/index.php/vl/article/view/5878) - Matthew O. Peterson, PHD
  + [Typographic Layout and First  Impressions - Testing how changes in text layout influence reader's judgments of documents](https://journals.uc.edu/index.php/vl/article/view/5879) - Jeanne-Louise Moys
  + [(mis)understanding: icon comprehension in different cultural contexts](https://journals.uc.edu/index.php/vl/article/view/5880) - Mike Zender, Amy Cassedy
- [2014 - Vol. 47 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/454)
  + [Moving Beyond "Just Making Things": Design History in the Studio and the Survey Classroom](https://journals.uc.edu/index.php/vl/article/view/5872) - Dori Griffin
  + [Critical Writing Strategies to Improve Class Critiques](https://journals.uc.edu/index.php/vl/article/view/5873) - Jillian Coorey, Gretchen Caldwell Rinnert
  + [Letterpress: Looking Backward to Look Forward](https://journals.uc.edu/index.php/vl/article/view/5874) - Alexander Cooper, Rose Gridneff, Andrew Haslam
  + [The Influence of Serifs on 'h' and 'i': Useful Knowledge from Design-led Scientific Research](https://journals.uc.edu/index.php/vl/article/view/5875) - Mary C. Dyson, Sofie Beier
  + [Investigating Readers' Impressions of Typographic Differentiation Using Repertory Grids](https://journals.uc.edu/index.php/vl/article/view/5876) - Jeanne-Louise Moys
- [2013 - Vol. 47 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/453)
  + [A Study on the Revelations of Design Students' Thinking Styles in Reflective Journals](https://journals.uc.edu/index.php/vl/article/view/5869) - Aruna Venkatesh
  + [Designing a Questionnaire to Gather Carer Input Pain Assessment for Hospitalized People w Dementia](https://journals.uc.edu/index.php/vl/article/view/5870) - Alison Black, Annette Gibb, Clare Carey, Sarah Barker, Claire Leake, Luke Solomons
  + [Making Cancer Surveillance Data More Accessible for the Public Through Dataspark](https://journals.uc.edu/index.php/vl/article/view/5871) - Heather Corcoran, Matthew Kreuter, Christina Clarke
- [2013 - Vol. 47 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/452)
  + [Visible Language in Transition](https://journals.uc.edu/index.php/vl/article/view/5865) - Sharon Poggenpohl, Paul Michael Zender
  + [Reflections on Teaching Research: A Conversation with Meredith Davis, Mary Dyson, Judith Gregory](https://journals.uc.edu/index.php/vl/article/view/5866) - Sharon Poggenpohl
  + [Bilingual Design Layout Systems: Cases from Beirut](https://journals.uc.edu/index.php/vl/article/view/5867) - Randa Abdel Baki
  + [Improving Icon Design: Through Focus on the Role of Individual Symbols in Construction of Meaning](https://journals.uc.edu/index.php/vl/article/view/5868) - Mike Zender, Mauricio Mejía
- [2012 - Vol. 46 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/451)
  + [Describing the Design of Children's Books: An analytical approach](https://journals.uc.edu/index.php/vl/article/view/5860) - Sue Walker
  + [How My Brain Stopped Reading](https://journals.uc.edu/index.php/vl/article/view/5861) - Sofie Beier
  + [Emotions in Typographic Design: An empirical examination](https://journals.uc.edu/index.php/vl/article/view/5862) - Beth E. Koch
  + [Composition Methodology of Optical Illusion Design](https://journals.uc.edu/index.php/vl/article/view/5863) - Regina W.Y. Wang, Chiung-Fen Wang
  + [Documents as 'Critical Incidents' in Organization to Consumer Communication](https://journals.uc.edu/index.php/vl/article/view/5864) - Alison Black, Karen L. Stanbridge
- [2012 - Vol. 46 - No. 1-2](https://journals.uc.edu/index.php/vl/issue/view/450)
  + [Models of Design: Envisioning a Future Design Education](https://journals.uc.edu/index.php/vl/article/view/5859) - Ken Friedman
  + [Envisioning a Future Design Education, Introduction](https://journals.uc.edu/index.php/vl/article/view/5850) - Sharon Poggenpohl
  + [Communication Design Education: Could Nine Reflections be Sufficient?](https://journals.uc.edu/index.php/vl/article/view/5851) - Karel van der Waarde, Maurits Vroombout
  + [What's Missing in Design Education Today?](https://journals.uc.edu/index.php/vl/article/view/5852) - Jorge Frascara, Guillermina Noël
  + [Design or "Design"—Envisioning a Future Design Education](https://journals.uc.edu/index.php/vl/article/view/5853) - David Sless
  + [Good Design is Good Social Change: Envisioning an Age of Accountability in Communication Design](https://journals.uc.edu/index.php/vl/article/view/5854) - Audrey Bennett
  + [Learn from the Core—Design from the Core](https://journals.uc.edu/index.php/vl/article/view/5855) - Thomas Ockerse
  + [Handsomely, Handsomely Now! 5 Impromptus for the Early Part of the Century](https://journals.uc.edu/index.php/vl/article/view/5856) - Chris Myers
  + [Leveraging Graduate Education for a More Relevant Future](https://journals.uc.edu/index.php/vl/article/view/5857) - Meredith Davis
  + [The Perennial and the Particular Challenges of Design Education](https://journals.uc.edu/index.php/vl/article/view/5858) - Stan Ruecker
- [2011 - Vol. 45 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/449)
  + [Interactive Visualizations of Plot in Fiction](https://journals.uc.edu/index.php/vl/article/view/5847) - Teresa Dobson, Piotr Michura, Stan Ruecker, Monica Brown, Omar Rodriguez,
  + [Do Designers Show Categorical Perception of Typefaces?](https://journals.uc.edu/index.php/vl/article/view/5848) - Mary C. Dyson,
  + [Critique: A Communicative Event in Design Education — A Qualitative Research](https://journals.uc.edu/index.php/vl/article/view/5849) - Ho Lan Helena Wong
- [2011 - Vol. 45 - No. 1-2](https://journals.uc.edu/index.php/vl/issue/view/448)
  + [Seeing Punctuation](https://journals.uc.edu/index.php/vl/article/view/5840) - Anne Toner
  + [From Invisibility to Visibility and Backwards: Punctuation in Comics](https://journals.uc.edu/index.php/vl/article/view/5841) - Jacques Dürrenmatt
  + [Necessary Smileys & Useless Periods: Redefining Punctuation in Electronically-Mediated Communication](https://journals.uc.edu/index.php/vl/article/view/5842) - Naomi S. Baron, Rich Ling
  + [Seven- to Nine-year-olds' Understandings of Speech Marks: Some issues and problems](https://journals.uc.edu/index.php/vl/article/view/5843) - Nigel Hall, Sue Sing
  + [Early-Modern "Speech" Marks](https://journals.uc.edu/index.php/vl/article/view/5844) - Nick Blackburn
  + [In/visible Punctuation](https://journals.uc.edu/index.php/vl/article/view/5845) - John Lennard
  + [Marks, Spaces and Boundaries: Punctuation (and other effects) in the typography of dictionaries](https://journals.uc.edu/index.php/vl/article/view/5846) - Paul Luna
- [2010 - Vol. 44 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/447)
  + [Audience/online Information Interactions:  New Research in Learning Preferences](https://journals.uc.edu/index.php/vl/article/view/5836) - Michèle Wong Kung Fong
  + [The Appropriateness of Icon Representations  for Taiwanese Computer Users](https://journals.uc.edu/index.php/vl/article/view/5837) - Hsiu-Feng Wang
  + [The Development of Automobile Speedometer Dials](https://journals.uc.edu/index.php/vl/article/view/5838) - Marilyn Mitchell
  + [Helvetica, the Film and the Face in Context](https://journals.uc.edu/index.php/vl/article/view/5839) - Dietmar R. Winkler
- [2010 - Vol. 44 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/446)
  + [Global Interaction in Design](https://journals.uc.edu/index.php/vl/article/view/5830) - Audrey Grace Bennett
  + [How Print Culture Came to Be Indigenous](https://journals.uc.edu/index.php/vl/article/view/5831) - Stuart McKee
  + [Navigating Cross-cultures, Curriculum and Confrontation](https://journals.uc.edu/index.php/vl/article/view/5832) - Audra Buck-Coleman
  + [Beyond Borders: Participatory Design Research and the Changing Role of Design](https://journals.uc.edu/index.php/vl/article/view/5833) - Adream Blair
  + [Virtual Conferencing in Global Design Education: Dreams and Realities](https://journals.uc.edu/index.php/vl/article/view/5834) - Judith A. Moldenhauer
  + [The New School Collaborates](https://journals.uc.edu/index.php/vl/article/view/5835) - Cynthia Lawson
- [2010 - Vol. 44 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/445)
  + [Antinomies of Semiotics in Graphic Design](https://journals.uc.edu/index.php/vl/article/view/5825) - Peter Storkerson
  + [Visual Communication for Medicines: Malignant Assumptions and Benign Design?](https://journals.uc.edu/index.php/vl/article/view/5826) - Karel van der Waarde
  + [Failure to Manage Constant Change](https://journals.uc.edu/index.php/vl/article/view/5827) - Ann McDonald
  + [Ibn Battuta: Edutaining the World?](https://journals.uc.edu/index.php/vl/article/view/5828) - Chae Ho Lee
  + [In Closing…What have we learned from failure?](https://journals.uc.edu/index.php/vl/article/view/5829) - Dietmar R. Winkler, Sharon Poggenpohl
- [2009 - Vol. 43 - No. 2-3](https://journals.uc.edu/index.php/vl/issue/view/444)
  + [Celebrating Failure](https://journals.uc.edu/index.php/vl/article/view/5817) - Dietmar R. Winkler, Sharon Poggenpohl
  + ['Realist' Stakeholder Analysis in Design](https://journals.uc.edu/index.php/vl/article/view/5818) - Mike Doherty
  + [Product Communication, Form, Failure and Safety](https://journals.uc.edu/index.php/vl/article/view/5819) - Len D. Singer
  + [Multiple Information Failure: A Case of Different Investments in Form and Content in Graphic Design](https://journals.uc.edu/index.php/vl/article/view/5820) - Carolyn Barnes, Simone Taffe, Lucy Miceli
  + [Lessons from Three Mile Island: Visual Design in a High-stakes Environment](https://journals.uc.edu/index.php/vl/article/view/5821) - Axel Roesler
  + [Paper Prototypes and Beyond](https://journals.uc.edu/index.php/vl/article/view/5822) - Stephen Brown
  + [Recognizing Risk-of-failure in Communication  Design Projects](https://journals.uc.edu/index.php/vl/article/view/5823) - Joyce Yee, Matthew Lieveslay, Louise Taylor
  + [Failure? Isn't it Time to Slay the Design-Dragon?](https://journals.uc.edu/index.php/vl/article/view/5824) - Dietmar Winkler
- [2009 - Vol. 43 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/443)
  + [Visual Culture and Visual Communication in the Context of Globalization](https://journals.uc.edu/index.php/vl/article/view/5814) - Dietmar Winkler
  + [Thinkeringspace: Designing for Collaboration (around the book and beyond)](https://journals.uc.edu/index.php/vl/article/view/5815) - Heloisa Moura, Dale Fahnstrom, Greg Prygrocki, T.J. McLeish
  + [A Mandala Browser User Study: Visualizing XML Versions of Shakespeare's Plays](https://journals.uc.edu/index.php/vl/article/view/5816) - Stéfan Sinclair, Stan Ruecker, Rhiannon Gainor, Matt Patey, Sandra Gabriele
- [2008 - Vol. 42 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/442)
  + [Design Literacy, Discourse and Communities of Practice](https://journals.uc.edu/index.php/vl/article/view/5810) - Sharon Helmer Poggenpohl
  + [Palimpsest: The Future of the Past](https://journals.uc.edu/index.php/vl/article/view/5811) - Janie Chun Nei Poon
  + [Understanding Diagrams: A Pointer to the Development of Diagramming Software](https://journals.uc.edu/index.php/vl/article/view/5812) - Kamaran Fathulla
  + [The New Way of Making Fonts with DTL Font Master](https://journals.uc.edu/index.php/vl/article/view/5813) - Nikola Djurek
- [2008 - Vol. 42 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/441)
  + [Skittish Skirts and Scanty Silhouettes: The Tribulations of Gender in Modern Signage](https://journals.uc.edu/index.php/vl/article/view/5806) - Pedro Bessa
  + [Language Impairment, Family Interaction and the Design of a Game](https://journals.uc.edu/index.php/vl/article/view/5807) - Guillerminna Noël
  + [Otto Neurath's Isotype and the Rhetoric of Neutrality](https://journals.uc.edu/index.php/vl/article/view/5808) - Jae Young Lee
  + [Anatomy of an Arabetic Type Design](https://journals.uc.edu/index.php/vl/article/view/5809) - Saad D. Abulhab
- [2008 - Vol. 42 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/440)
  + [After the Grave: Language and Materiality](https://journals.uc.edu/index.php/vl/article/view/5801) - David Scott Armstrong, Patrick Mahon
  + [Sfumato or, Print: Like a Vanishing Point Grown Over by its Picture Plane](https://journals.uc.edu/index.php/vl/article/view/5802) - David Scott Armstrong
  + [Xu Bing, Ed Pien and Gu Xiong: Lost and Found in Translation](https://journals.uc.edu/index.php/vl/article/view/5803) - Patrick Mahon
  + [After the Death of Film: Writing the Natural world in the Digital Age](https://journals.uc.edu/index.php/vl/article/view/5804) - Tess Takahashi
  + [Artist's Projects](https://journals.uc.edu/index.php/vl/article/view/5805) - Patrick Mahon, David Scott Armstrong, Blair Brennan, Barbara Balfour, Joscelyn Gardner, Micah Lexier, David Merritt, Jeannie Thib
- [2007 - Vol. 41 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/439)
  + [Visual Metaphors in User Instructions](https://journals.uc.edu/index.php/vl/article/view/5795) - Karel van der Waarde, Piet Westendorp
  + [The Use of Metaphors in Dietary Visual Displays Around the World](https://journals.uc.edu/index.php/vl/article/view/5796) - Isabel Meirelles
  + [Representations of Time in Computer Interface Design](https://journals.uc.edu/index.php/vl/article/view/5797) - Marilyn Mitchel, Peter van Sommers
  + [Italicization and Understanding Texts through Metaphoric Projections of Movement](https://journals.uc.edu/index.php/vl/article/view/5798) - Phil Jones
  + [The Method of Graphic Abstraction in Visual Metaphor](https://journals.uc.edu/index.php/vl/article/view/5799) - Regina W.Y. Wang, Chun Hsu Hsu
  + [A Schema for Depiction](https://journals.uc.edu/index.php/vl/article/view/5800) - Stephen Boyd Davis
- [2007 - Vol. 41 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/438)
  + [Designing Philosophy](https://journals.uc.edu/index.php/vl/article/view/5792) - David Sless
  + [The Homogenized Imagery of Non-Profit Organizations on the Internet](https://journals.uc.edu/index.php/vl/article/view/5793) - Linda Jean Kenix
  + [Relating the Visual and the Headline in Chinese Print Advertisements](https://journals.uc.edu/index.php/vl/article/view/5794) - Lawrence Chun-wai Yu
- [2007 - Vol. 41 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/437)
  + [Design of a Rich-Prospect Browsing Interface for Seniors](https://journals.uc.edu/index.php/vl/article/view/5788) - Stan Ruecker, Lisa M. Given, Elizabeth Sadler, Andrea Ruskin, Heather Simpson
  + [Visual Language for the Expression of Scientific Concepts](https://journals.uc.edu/index.php/vl/article/view/5789) - Mike Zender, Keith A. Crutcher
  + [Binding the Electronic Book: Design Features for Bibliophiles](https://journals.uc.edu/index.php/vl/article/view/5790) - Stan Ruecker, Kirsten C. Uszkalo
  + [Alphabet ante portas: How English Text Invades Japanese Public Space](https://journals.uc.edu/index.php/vl/article/view/5791) - Peter Backhaus
- [2006 - Vol. 40 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/436)
  + [Children's Responses to Line Spacing in Early Reading Books or 'Holes to tell which line you're on'](https://journals.uc.edu/index.php/vl/article/view/5784) - Sue Walker, Linda Reynolds, Allison Duncan
  + [Analyzing Multimodal Interaction within a Classroom Setting](https://journals.uc.edu/index.php/vl/article/view/5785) - Heloisa Moura
  + [Typography Behind the Arabetic Calligraphy Veil](https://journals.uc.edu/index.php/vl/article/view/5786) - Saad D. Abulhab
  + [Comparison of Maya and Oracle Bone Scripts](https://journals.uc.edu/index.php/vl/article/view/5787) - William Chiang
- [2006 - Vol. 40 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/435)
  + [System, Suspension, Seduction: Anne Bush's Critical Design Practice](https://journals.uc.edu/index.php/vl/article/view/5780) - John Zuern
  + [Between Script and Pictures in Japan](https://journals.uc.edu/index.php/vl/article/view/5781) - Barrie Shelton, Emiko Okayama
  + [Advancing Icon Design for Global Nonverbal Communication: or What does the word 'bow' mean?](https://journals.uc.edu/index.php/vl/article/view/5782) - Mike Zender
  + [Teaching Design: Analysis from Three Different Analytical Perspectives](https://journals.uc.edu/index.php/vl/article/view/5783) - Eric Swanson, Stacie Sabady, Chris Yin
- [2006 - Vol. 40 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/434)
  + [The Dialectics of Legacy](https://journals.uc.edu/index.php/vl/article/view/5773) - Ken Friedman, Owen F. Smith
  + [What Has Fluxus Created?](https://journals.uc.edu/index.php/vl/article/view/5774) - Ann Klefstad
  + [Introductions: Signatures, Music, Computers, Paranoia, Smells, Danger & the Sky](https://journals.uc.edu/index.php/vl/article/view/5775) - Lisa Moren
  + [Artists' Statements](https://journals.uc.edu/index.php/vl/article/view/5776) - -- Various
  + [Games as Art: The Aesthetics of Play](https://journals.uc.edu/index.php/vl/article/view/5777) - Celia Pearce
  + [The Literature of Fluxus](https://journals.uc.edu/index.php/vl/article/view/5778) - Ken Friedman
  + [A Fluxus Bibliography](https://journals.uc.edu/index.php/vl/article/view/5779) - Ken Friedman, Owen F. Smith
- [2005 - Vol. 39 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/433)
  + [Teaching and Learning about Fluxus](https://journals.uc.edu/index.php/vl/article/view/5768) - Owen F. Smith
  + [Fluxus — Reference or p Paradigm for Young Contemporary Artists?](https://journals.uc.edu/index.php/vl/article/view/5769) - Bertand Clavez
  + [Fluxkids (Overview)](https://journals.uc.edu/index.php/vl/article/view/5770) - Hannah Higgins
  + [Fluxus Futures, Ben Vautier's Signature Acts and the Historiography of the Avant-garde](https://journals.uc.edu/index.php/vl/article/view/5771) - Ina Bloom
  + [History, Historiography and Legacy](https://journals.uc.edu/index.php/vl/article/view/5772) - Ken Friedman, Owen F. Smith
- [2005 - Vol. 39 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/432)
  + [Dynamic Visual Formation](https://journals.uc.edu/index.php/vl/article/view/5764) - Isabel Meirelles
  + [Optimal Line Length in Reading — A Literature Review](https://journals.uc.edu/index.php/vl/article/view/5765) - Anuj A. Nanavati, Randolph G. Bias
  + [Rationalizing Design Sensitivity](https://journals.uc.edu/index.php/vl/article/view/5766) - Mandar S. Rane
  + [UnReading William Blake's Marginalia](https://journals.uc.edu/index.php/vl/article/view/5767) - Jason Snart
- [2005 - Vol. 39 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/431)
  + [Existential Textuality](https://journals.uc.edu/index.php/vl/article/view/5760) - Emily McVarish
  + [Multi-level Document Visualization](https://journals.uc.edu/index.php/vl/article/view/5761) - Stan Ruecker, Eric Homich, Stéfan Sinclair
  + [Re-design in Public Space](https://journals.uc.edu/index.php/vl/article/view/5762) - Fedja Vukic
  + [Re-design in Public Space: The Work](https://journals.uc.edu/index.php/vl/article/view/5763) - Eduard Cehovin
- [2004 - Vol. 38 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/430)
  + [Towards a Reader-friendly Font](https://journals.uc.edu/index.php/vl/article/view/5757) - Larry D. Reid, Meta Reid, Audrey Bennett
  + [Left-handedness: A Writing Handicap?](https://journals.uc.edu/index.php/vl/article/view/5758) - Ian Peachey
  + [Thinking on Paper: Hindu-Arabic Numerals  in European Typography](https://journals.uc.edu/index.php/vl/article/view/5759) - Liz C. Throop
- [2004 - Vol. 38 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/429)
  + [Practicing Collaboration in Design](https://journals.uc.edu/index.php/vl/article/view/5752) - Sharon Helmer Poggenpohl
  + [A Case Study in Collaboration: Looking back at the National Graphic Design Archive](https://journals.uc.edu/index.php/vl/article/view/5753) - R. Roger Remington
  + [User Studies: Finding a Place in Design Practice and Education](https://journals.uc.edu/index.php/vl/article/view/5754) - Jay Melican
  + [Where Are the Design Methodologists?](https://journals.uc.edu/index.php/vl/article/view/5755) - Chris Conley
  + [Perspectives of Design Research: Collective Views for Forming the Foundation of Design Research](https://journals.uc.edu/index.php/vl/article/view/5756) - Keiichi Sato
- [2004 - Vol. 38 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/428)
  + [Seeing and the Mixtec Screenfolds](https://journals.uc.edu/index.php/vl/article/view/5750) - Byron Hamann
  + [A Visible Language Analysis of User-Interface Design Components and Culture Dimensions](https://journals.uc.edu/index.php/vl/article/view/5751) - Aaron Marcus, Valentina Johanna Baumgartner
- [2003 - Vol. 37 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/427)
  + [Aesthetic or AnAesthetic: The Competing Symbols of Las Vegas Strip](https://journals.uc.edu/index.php/vl/article/view/5745) - Ritu Bhatt
  + ["Doing It Deadpan:" Venturi, Scott Brown and Izenour’s Learning from Las Vegas](https://journals.uc.edu/index.php/vl/article/view/5746) - Michael Golec
  + [Skepticism and the Ordinary — From Burnt Norton To Las Vegas](https://journals.uc.edu/index.php/vl/article/view/5747) - Aron Vinegar
  + [Learning from Las Vegas… and Los Angeles and Reyner Banham](https://journals.uc.edu/index.php/vl/article/view/5748) - Nigel Whiteley
  + [Signs Taken for Wonders](https://journals.uc.edu/index.php/vl/article/view/5749) - Dell Upton
- [2003 - Vol. 37 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/426)
  + [Cultural Dimensions of Visual Communication: An Introduction](https://journals.uc.edu/index.php/vl/article/view/5739) - Sharon Helmer Poggenpohl
  + [Function and Expression: Student Typographic Work in English and Indian languages](https://journals.uc.edu/index.php/vl/article/view/5740) - Martha Scotford
  + [The Metastructural Dynamics of Interactive Electronic Design](https://journals.uc.edu/index.php/vl/article/view/5741) - Patricia Search
  + [Mapping a Graphic Genome: A Cross-Cultural Comparison between Korean and Japanese Graphic Designers](https://journals.uc.edu/index.php/vl/article/view/5742) - Min-Soo Kim
  + [Toilet Signage as Effective Communication](https://journals.uc.edu/index.php/vl/article/view/5743) - Lynn Ciochetto
  + [Graphic Assimilation: New Immigrants and Social Identity](https://journals.uc.edu/index.php/vl/article/view/5744) - Barbara Martinson, Sauman Chu
- [2003 - Vol. 37 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/425)
  + [Developing Theory, Analysis and Effective Research Communication for Design](https://journals.uc.edu/index.php/vl/article/view/5734) - Sharon Helmer Poggenpohl
  + [Designing Theory in Communication](https://journals.uc.edu/index.php/vl/article/view/5735) - Peter Storkerson
  + [Value-added Text: Where Graphic Design Meets Paralinguistics](https://journals.uc.edu/index.php/vl/article/view/5736) - Stuart Mealing
  + [User Analysis Framework: Thoughts on User’s Cognitive Factors for Information Design on Web-based Me](https://journals.uc.edu/index.php/vl/article/view/5737) - Napawan Sawasdichai, Sharon Poggenpohl
  + [Get Real: The Need for Effective Design Research](https://journals.uc.edu/index.php/vl/article/view/5738) - Christopher Nemeth
- [2002 - Vol. 36 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/424)
  + [Methodology for Uncovering Motion Affordance in Interactive Media](https://journals.uc.edu/index.php/vl/article/view/5730) - Chujit Jeamsinkul, Sharon Poggenpohl
  + [Methods for Manipulating Electronic Documents in Relation to Information Retrieval](https://journals.uc.edu/index.php/vl/article/view/5731) - Mária González de Cosio, Mary C. Dyson
  + [Communicating Cuneiform: The Evolution of a Multimedia Cuneiform Database](https://journals.uc.edu/index.php/vl/article/view/5732) - S.I. Woolley, T.R. Davis, N.J. Flowers, J. Pinilla-Dutoit, A. Livingstone, T.N. Arvanitis
  + [Typographic Cueing on Screen](https://journals.uc.edu/index.php/vl/article/view/5733) - Mary C. Dyson, Judy Gregory
- [2002 - Vol. 36 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/423)
  + [Annotated Design Research Bibliography Process Overview](https://journals.uc.edu/index.php/vl/article/view/5725) - Praima Chayutsahakij
  + [Perspectives on Building a Philosophy of Design](https://journals.uc.edu/index.php/vl/article/view/5726) - Sakol Teeravarunyou, Carlos Teixeira
  + [Perspectives on Building a Foundation for Design Research](https://journals.uc.edu/index.php/vl/article/view/5727) - Chujit Jeamsinkul, Napawan Sawasdichai
  + [Perspectives on Building a Discourse Between Design Theory and Practice](https://journals.uc.edu/index.php/vl/article/view/5728) - Praima Chayutsahakij
  + [Cultivating an Interest in Design Research](https://journals.uc.edu/index.php/vl/article/view/5729) - Sharon Helmer Poggenpohl
- [2002 - Vol. 36 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/422)
  + [Alix Lambert's "The Mark of Cain"](https://journals.uc.edu/index.php/vl/article/view/5722) - Michael Golec
  + [Visual Design of Interactive Software for Older Adults: Preventing Drug Interactions in Older Adults](https://journals.uc.edu/index.php/vl/article/view/5723) - Zoe Strickler, Patricia Neafsey
  + [The Mirage Project: An Experimental Qualitative Reception Study](https://journals.uc.edu/index.php/vl/article/view/5724) - Bruno Ingemann
- [2001 - Vol. 35 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/421)
  + [Some Things That Pictures are Good For: An Information Processing Perspective](https://journals.uc.edu/index.php/vl/article/view/5718) - Lester Loschky
  + [Limits of Language, Limits of World](https://journals.uc.edu/index.php/vl/article/view/5719) - Dietmar Winkler
  + [Critical Viewing of Television](https://journals.uc.edu/index.php/vl/article/view/5720) - Matthew McClain
  + [New Media, Experience and Japanese Way of Tea (Chado)](https://journals.uc.edu/index.php/vl/article/view/5721) - Jennifer Gunji
- [2001 - Vol. 35 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/420)
  + [Surrogate Multiplicities: Typography in the Age of Invisibility](https://journals.uc.edu/index.php/vl/article/view/5715) - Katie Salen
  + [Diagramming as a Way of Thinking Ecologically](https://journals.uc.edu/index.php/vl/article/view/5716) - Jorge Frascara
  + [Effects of Minimal Legible Size Characters on Chinese Word Recognition](https://journals.uc.edu/index.php/vl/article/view/5717) - Sheng-Hsiung Hsu, Kuo-Chen Huang
- [2001 - Vol. 35 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/419)
  + [The Word of Poetry, Sounds of the Voice and Technology](https://journals.uc.edu/index.php/vl/article/view/5705) - Mario Costa
  + [True Heritage: The Sound Image in Experimental Poetry](https://journals.uc.edu/index.php/vl/article/view/5706) - Harry Polkinhorn
  + [Technology, Polypoetry and the Aura of Poly-performance](https://journals.uc.edu/index.php/vl/article/view/5707) - Nicholas Zurbrugg
  + [Visible Language, Audible Language, Inarticulable Language and the "Supplementary Signifier"](https://journals.uc.edu/index.php/vl/article/view/5708) - Nicholas Zurbrugg
  + [A Soundscape of Contemporary Hungarian Poetry](https://journals.uc.edu/index.php/vl/article/view/5709) - Endre Szkárosi
  + [Experimental Poetics Based on Sound Poetry Today](https://journals.uc.edu/index.php/vl/article/view/5710) - Philadelpho Menezes
  + [Experimental Poetry in Barcelona during the 1990s](https://journals.uc.edu/index.php/vl/article/view/5711) - Lis Cost
  + [Relations Between Sound Poetry and Visual Poetry, The Path from the Optophonetic Poem to the Multime](https://journals.uc.edu/index.php/vl/article/view/5712) - Christian Scholz
  + [The Singing Blackbird, voice, images, technology in polypoetry](https://journals.uc.edu/index.php/vl/article/view/5713) - Enzo Minarelli
  + [The Manifesto of Polypoetry](https://journals.uc.edu/index.php/vl/article/view/5714) - Enzo Minarelli
- [2000 - Vol. 34 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/418)
  + [The Gloss as Poetics, Transcending the Didactic](https://journals.uc.edu/index.php/vl/article/view/5700) - Kyoko Takahasi Wilkerson, Douglas Wilkerson
  + [A Typography of Impoverishment: D.C. McMurtrie’s Reception of European Modernist Typography and an A](https://journals.uc.edu/index.php/vl/article/view/5701) - Michael Golec
  + [Using Lists to Improve Text Access: The Role of Layout in Reading](https://journals.uc.edu/index.php/vl/article/view/5702) - Yusaku Seki
  + [Portrait or Landscape? Typographical Layouts for Patient Information Leaflets](https://journals.uc.edu/index.php/vl/article/view/5703) - James Hartley, Matthew Johnson
  + [Making Books: A Review and Critical Commentary](https://journals.uc.edu/index.php/vl/article/view/5704) - Colin Banks
- [2000 - Vol. 34 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/417)
  + [Words in Space: An Introduction](https://journals.uc.edu/index.php/vl/article/view/5695) - Sharon Helmer Poggenpohl
  + [The Restaurant at This End of the Universe: Edible Typography in New Zealand](https://journals.uc.edu/index.php/vl/article/view/5696) - Sydney Shep
  + [South of the Border](https://journals.uc.edu/index.php/vl/article/view/5697) - Maria Rogal
  + [Word Space / Book Space / Poetic Space, Experiments in Transformation](https://journals.uc.edu/index.php/vl/article/view/5698) - Lucinda Hitchcock
  + [Reflections on Words in Space](https://journals.uc.edu/index.php/vl/article/view/5699) - Sharon Helmer Poggenpohl
- [2000 - Vol. 34 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/416)
  + [Words in Space: An Introduction](https://journals.uc.edu/index.php/vl/article/view/5690) - Sharon Helmer Poggenpohl
  + [Preserving Words: The Korean Tripitaka](https://journals.uc.edu/index.php/vl/article/view/5691) - Sharon Helmer Poggenpohl, Sang-Soo Ahn
  + [Bookcover as Intertitle in the Cinema of Jean-Luc Godard](https://journals.uc.edu/index.php/vl/article/view/5692) - Kevin Hayes
  + [Reading the City: Writing and the Construction of Urban Space in Jem Cohen’s Lost Book Found](https://journals.uc.edu/index.php/vl/article/view/5693) - Mark Owens
  + [Visualizing Place](https://journals.uc.edu/index.php/vl/article/view/5694) - Andrea Wollensak
- [1999 - Vol. 33 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/415)
  + ["An Eccentric Reversible Reaction": Yi Sang's Experimental Poetry in the 1930s and Its Meaning to Contemporary Design](https://journals.uc.edu/index.php/vl/article/view/5686) - Min-Soo Kim
  + [Roxane, A Study in Visual Factors Effecting Legibility](https://journals.uc.edu/index.php/vl/article/view/5687) - Stuart Gluth
  + [Zealand, Reflections on Developing a Typeface](https://journals.uc.edu/index.php/vl/article/view/5688) - Charles Gibbons
  + [CurioCity, Developing an "Active Learning" Game](https://journals.uc.edu/index.php/vl/article/view/5689) - Lynne Ferguson
- [1999 - Vol. 33 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/414)
  + [Toward Disambiguating the Term "Roman"](https://journals.uc.edu/index.php/vl/article/view/5682) - Earl Herrick
  + [Becoming-zoa](https://journals.uc.edu/index.php/vl/article/view/5683) - Ron Broglio
  + [The Work of Art in the Age of Digital Reproduction](https://journals.uc.edu/index.php/vl/article/view/5684) - Markus Hallensleben
  + [What’s Been Cooking in the Type Kitchen](https://journals.uc.edu/index.php/vl/article/view/5685) - Colin Banks
- [1999 - Vol. 33 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/413)
  + [Thirty Years of Visible Writing: A Memoir](https://journals.uc.edu/index.php/vl/article/view/5678) - Richard Kostelanetz
  + [ASCII Classroom, A text dependent investigation of the studio art classroom](https://journals.uc.edu/index.php/vl/article/view/5679) - Laural Beth Beckman
  + [Axis — a line about which a body, Contextualizing photo-text work](https://journals.uc.edu/index.php/vl/article/view/5680) - Brigid McLeer
  + [Cognition, Emotion and Other Inescapable Dimensions of Human Experience](https://journals.uc.edu/index.php/vl/article/view/5681) - Jorge Frascara
- [1998 - Vol. 32 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/412)
  + [Pintaderas, Pre-Columbian Stamps](https://journals.uc.edu/index.php/vl/article/view/5672) - Claudia Navarro Tapia
  + [Rhetoric in Logotypes](https://journals.uc.edu/index.php/vl/article/view/5673) - Mária González de Cosio
  + [The Peso, National Currency as Rhetoric](https://journals.uc.edu/index.php/vl/article/view/5674) - Martha Salazar
  + [Visual Rhetoric: An Introduction](https://journals.uc.edu/index.php/vl/article/view/5675) - Sharon Helmer Poggenpohl
  + [Doubly Damned, Rhetorical and Visual](https://journals.uc.edu/index.php/vl/article/view/5676) - Sharon Helmer Poggenpohl
  + [Tale of the Origin,  A Rhetorical Visual Analysis of a Mexican Mythical Tale](https://journals.uc.edu/index.php/vl/article/view/5677) - Alejandro Brizuela
- [1998 - Vol. 32 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/411)
  + [Extending Control of Digital Typography](https://journals.uc.edu/index.php/vl/article/view/5669) - Peter Karow
  + [Customized Digital Books on Demand, Issues in the creation of a flexible document format](https://journals.uc.edu/index.php/vl/article/view/5670) - Karsten Lücke
  + [The Effects of Line Length and Method of Movement on Patterns of Reading from Screen](https://journals.uc.edu/index.php/vl/article/view/5671) - Mary C. Dyson, Gary J. Kipping
- [1998 - Vol. 32 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/410)
  + [Twenty-Six Not-So-Easy Pieces](https://journals.uc.edu/index.php/vl/article/view/5665) - Sharon Helmer Poggenpohl
  + [Writing in the Age of Email: The Impact of Ideology versus Technology](https://journals.uc.edu/index.php/vl/article/view/5666) - Naomi S. Baron
  + [Paper Representations of the Non-Standard Voice](https://journals.uc.edu/index.php/vl/article/view/5667) - Mark Balhorn
  + [Cloth-Bound Reverie](https://journals.uc.edu/index.php/vl/article/view/5668) - Michael Golec
- [1997 - Vol. 31 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/409)
  + [Dis[appearances]: Representational Strategies and Operational Needs in Codexspace and Screenspace](https://journals.uc.edu/index.php/vl/article/view/5661) - Sharyn O’Mara, Katie Salen
  + [Early Greek Typography in Milan, A Historical Note on a New Greek Typeface](https://journals.uc.edu/index.php/vl/article/view/5662) - Martin Wallraff
  + [Teaching Critical Analytical Methods in the Digital Typography Classroom](https://journals.uc.edu/index.php/vl/article/view/5663) - Michael Gibson
  + [On the Effectiveness of Highlighting Ads in Telephone Directories by Color](https://journals.uc.edu/index.php/vl/article/view/5664) - Dirk Wendt, Wiebke Groggel, Georg Gutschmidt
- [1997 - Vol. 31 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/408)
  + [Loss of an Empire/Gaining Another?](https://journals.uc.edu/index.php/vl/article/view/5655) - Dietmar Winkler
  + [Hypertext & The Art of Memory](https://journals.uc.edu/index.php/vl/article/view/5656) - Peter Storkerson, Janine Wong
  + [Exploring the Special Communications Experiences of Online Education](https://journals.uc.edu/index.php/vl/article/view/5657) - Greg Stone
  + [A Conceptual Model of Interactive Exhibits for African-American Children](https://journals.uc.edu/index.php/vl/article/view/5658) - Thomas Rasheed, Leif Allmendinger
  + [Inclusive Interaction: Ability Enhancing Multimedia Design](https://journals.uc.edu/index.php/vl/article/view/5659) - Carl Twarog
  + [Virtual Avatars: Subjectivity in Virtual Environments](https://journals.uc.edu/index.php/vl/article/view/5660) - Diana J. Gromala
- [1997 - Vol. 31 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/407)
  + [Thinking, Learning and the Written Word](https://journals.uc.edu/index.php/vl/article/view/5650) - Naomi S. Baron
  + [How Can I Be Literate: Counting the Ways](https://journals.uc.edu/index.php/vl/article/view/5651) - Dianne G. Kanawati
  + [Puns, Public Discourse and Postmodernism](https://journals.uc.edu/index.php/vl/article/view/5652) - Brock Haussamen
  + [The Book in American Utopia Literature, 1883-1917](https://journals.uc.edu/index.php/vl/article/view/5653) - Kevin J.   Hayes
  + [Simply a Dot](https://journals.uc.edu/index.php/vl/article/view/5654) - Dermot McGuinne
- [1996 - Vol. 30 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/406)
  + [A Poetry-film Storyboard: Transformations](https://journals.uc.edu/index.php/vl/article/view/5645) - Richard Kostelanetz
  + [Better Information Presentation: Satisfying Customers?](https://journals.uc.edu/index.php/vl/article/view/5646) - David Sless
  + [Designing Bilingual Books for Children](https://journals.uc.edu/index.php/vl/article/view/5647) - Sue Walker, Viv Edwards, Ruth Blacksell
  + [The Apprenticeship Approach to Writing Instruction](https://journals.uc.edu/index.php/vl/article/view/5648) - Moti Nissani
  + [Jan Tschichold and the Language of Modernism](https://journals.uc.edu/index.php/vl/article/view/5649) - Peter Storkerson
- [1996 - Vol. 30 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/405)
  + [The Interactive Diagram Sentence: Hypertext as a Medium of Thought](https://journals.uc.edu/index.php/vl/article/view/5638) - Jim Rosenberg
  + [Poetic Machinations](https://journals.uc.edu/index.php/vl/article/view/5639) - Philippe Bootz
  + [Videopoetry](https://journals.uc.edu/index.php/vl/article/view/5640) - E. M. de Melo e Castro
  + [We Have not Understood Descartes](https://journals.uc.edu/index.php/vl/article/view/5641) - Andras Vallias
  + [Potentialities of Literary Cybertext](https://journals.uc.edu/index.php/vl/article/view/5642) - John Cayley
  + [Holopoetry](https://journals.uc.edu/index.php/vl/article/view/5643) - Eduardo Kac
  + [New Media Poetry — Theory and Strategies](https://journals.uc.edu/index.php/vl/article/view/5644) - Eric Vos
- [1996 - Vol. 30 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/404)
  + [How Efficient is the Chinese Writing System?](https://journals.uc.edu/index.php/vl/article/view/5635) - John DeFrancis
  + [Variability in Written Japanese: Towards a Sociolinguistics of Script Choice](https://journals.uc.edu/index.php/vl/article/view/5636) - Janet Shibamoto Smith, David L. Schmidt
  + [Between Picture and Proposition: Torturing Paintings in Wittgenstein’s Tractatus](https://journals.uc.edu/index.php/vl/article/view/5637) - James Elkins
- [1995 - Vol. 29 - No. 3-4](https://journals.uc.edu/index.php/vl/issue/view/403)
  + [Between Visual and Digital Tokens: A Look at the Abstraction of Money](https://journals.uc.edu/index.php/vl/article/view/5628) - Sharon Helmer Poggenpohl
  + [Flying Money](https://journals.uc.edu/index.php/vl/article/view/5629) - Dawn Barrett
  + ["Modest Enquiry" and Major Innovation: Franklin’s Early American Currency](https://journals.uc.edu/index.php/vl/article/view/5630) - Dawn Barrett
  + [J. S. G. Boggs: Life Size and in Color](https://journals.uc.edu/index.php/vl/article/view/5631) - Sandra Smith
  + [Interview with J. S. G. Boggs](https://journals.uc.edu/index.php/vl/article/view/5632) - Manuel Gonzalez
  + [Paper, Gold and Art as Representation and Exchange](https://journals.uc.edu/index.php/vl/article/view/5633) - Marc Shell
  + [Funny Money, Coupons, Scrip, Chips and Other Quasi-Official Media of Exchange](https://journals.uc.edu/index.php/vl/article/view/5634) - Christopher Nemeth
- [1995 - Vol. 29 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/402)
  + [The Luminous Object: Video Art and Video Theory: An Introduction](https://journals.uc.edu/index.php/vl/article/view/5618) - Hans Breder, Herman Rapaport
  + [Nam June Paik: An Interview](https://journals.uc.edu/index.php/vl/article/view/5619) - Nicholas Zurbrugg
  + [Film Image/Electronic Image: The Construction of Abstraction, 1960-1990](https://journals.uc.edu/index.php/vl/article/view/5620) - John G. Hanhardt
  + [Time and Light: David Garcia, An Interview](https://journals.uc.edu/index.php/vl/article/view/5621) - Herman Rapaport
  + [A Living Library: New Model for Global Electronic Interactivity and Networking in the Garden](https://journals.uc.edu/index.php/vl/article/view/5622) - Bonnie Sherk
  + [Television and the Unconscious, Donald Kuspit: An Interview](https://journals.uc.edu/index.php/vl/article/view/5623) - Herman Rapaport
  + [Literary Video](https://journals.uc.edu/index.php/vl/article/view/5624) - Richard Kostelanetz
  + [Carole Anne Klonarides: An Interview](https://journals.uc.edu/index.php/vl/article/view/5625) - Herman Rapaport
  + [Jameson’s Complaint Video-Art and the Intertextual "Time-Wall"](https://journals.uc.edu/index.php/vl/article/view/5626) - Nicholas Zurbrugg
  + [The Liminal Eye](https://journals.uc.edu/index.php/vl/article/view/5627) - Herman Rapaport
- [1995 - Vol. 29 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/401)
  + [How Long Has This Been Going On? Harpers Bazaar, Funny Face and the Construction of the Modernist Woman](https://journals.uc.edu/index.php/vl/article/view/5614) - Susan Sellers
  + [Embodiments of Human Identity: Detecting and Interpreting Hidden Narratives in Twentieth-Century Design History](https://journals.uc.edu/index.php/vl/article/view/5615) - Jack Williamson
  + [Alphabet Soup: Reading British Fanzines](https://journals.uc.edu/index.php/vl/article/view/5616) - Teal Triggs
  + [New Demotic Typography: The Search for New Indices](https://journals.uc.edu/index.php/vl/article/view/5617) - Frances Butler
- [1994 - Vol. 28 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/400)
  + [Culture is the Limit: Pushing the Boundaries of Graphic Design Criticism and Practice](https://journals.uc.edu/index.php/vl/article/view/5609) - Marilyn Crafton Smith
  + [Design and Reflexivity](https://journals.uc.edu/index.php/vl/article/view/5610) - Jan van Toorn
  + [Simulated Histories](https://journals.uc.edu/index.php/vl/article/view/5611) - Stuart McKee
  + [Deconstruction and Graphic Design: History Meets Theory](https://journals.uc.edu/index.php/vl/article/view/5612) - Ellen Lupton, J. Abbott Miller
  + [Messy History vs. Neat History: Toward an Expanded View of Women in Graphic Design](https://journals.uc.edu/index.php/vl/article/view/5613) - Martha Scotford
- [1994 - Vol. 28 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/399)
  + [An Opening: Graphic Design’s Discursive Spaces](https://journals.uc.edu/index.php/vl/article/view/5604) - Andrew Blauvelt
  + [Through the Looking Glass: Territories of the Historiographic Gaze](https://journals.uc.edu/index.php/vl/article/view/5605) - Anne Bush
  + [Narrative Problems of Graphic Design History](https://journals.uc.edu/index.php/vl/article/view/5606) - Victor Margolin
  + [A Poetics of Graphic Design?](https://journals.uc.edu/index.php/vl/article/view/5607) - Steve Baker
  + [Masks on Hire: In Search of Typographic Histories](https://journals.uc.edu/index.php/vl/article/view/5608) - Gérard Mermoz
- [1994 - Vol. 28 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/398)
  + [Recollect Orality](https://journals.uc.edu/index.php/vl/article/view/5599) - Eleanor O.   Close
  + [Can You See Whose Speech Is Overlapping?](https://journals.uc.edu/index.php/vl/article/view/5600) - Charles F. Meyer, Ed Blanchman, Robert A. Morris
  + [The Writing Problems of Visual Thinkers](https://journals.uc.edu/index.php/vl/article/view/5601) - Gerald Grow
  + [The News as a Post-Literary Spectacle](https://journals.uc.edu/index.php/vl/article/view/5602) - Joseph F. Keppler
  + [More than a Book Review of The Electronic Word](https://journals.uc.edu/index.php/vl/article/view/5603) - Sharon Helmer Poggenpohl
- [1994 - Vol. 28 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/397)
  + [The Future of the English Sentence](https://journals.uc.edu/index.php/vl/article/view/5595) - Brock Haussamen
  + [Problems that Face Research in the Design of Spelling](https://journals.uc.edu/index.php/vl/article/view/5596) - Valerie Yule
  + [The Unconsidered Ballot: How Design Effects Voting Behavior](https://journals.uc.edu/index.php/vl/article/view/5597) - Susan King Roth
  + [Blind Eyes, Innuendo and the Politics of Design (A Reply to Clive Chizlett)](https://journals.uc.edu/index.php/vl/article/view/5598) - Robin  Kinross
- [1993 - Vol. 27 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/396)
  + [Foreword and Introduction](https://journals.uc.edu/index.php/vl/article/view/5587) - Harry Polkinhorn
  + [Brazilian Visual Poetry](https://journals.uc.edu/index.php/vl/article/view/5588) - Philadelpho Menezes
  + [Cuban Visual Poetry](https://journals.uc.edu/index.php/vl/article/view/5589) - Pedro Juan Gutierrez
  + [Italian Visual Poetry](https://journals.uc.edu/index.php/vl/article/view/5590) - Enzo Minarelli
  + [Mexican Visual Poetry](https://journals.uc.edu/index.php/vl/article/view/5591) - César Espinosa
  + [Portuguese Visual Poetry](https://journals.uc.edu/index.php/vl/article/view/5592) - Fernando Aguiar
  + [Uruguayan Visual Poetry](https://journals.uc.edu/index.php/vl/article/view/5593) - Clemette Padin
  + [Visual Poetry from the United States](https://journals.uc.edu/index.php/vl/article/view/5594) - Harry Polkinhorn
- [1993 - Vol. 27 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/395)
  + [Speaking in Text: The Resonance of Syntactic Difference in Text Interpretation](https://journals.uc.edu/index.php/vl/article/view/5582) - Katie Salen
  + ["Te Tiriti" (The Treaty)](https://journals.uc.edu/index.php/vl/article/view/5583) - Max Hailstone
  + [Saving Pictures from the Flood: Using Visual Art in Creative Writing Workshops](https://journals.uc.edu/index.php/vl/article/view/5584) - Christopher Davis
  + [Graphic Cueing of Text: The Typographic and Diagraphic Dimensions](https://journals.uc.edu/index.php/vl/article/view/5585) - Charles T. Gilreath
  + [Proof-reading Monospaced and Proportionally-spaced Typefaces: Should We Check Typewritten or Typeset](https://journals.uc.edu/index.php/vl/article/view/5586) - Alison Black, Darren Watts
- [1993 - Vol. 27 - No. 1-2](https://journals.uc.edu/index.php/vl/issue/view/394)
  + [Seeing in Depth: the Practice of Bilingual Writing](https://journals.uc.edu/index.php/vl/article/view/5573) - Richard Hodgson, Ralph Sarkonak
  + [Literary Diglossia, Biculturalism and Cosmopolitanism in Literature](https://journals.uc.edu/index.php/vl/article/view/5574) - William Mackey
  + [Bilingual Babel: Cuneiform Texts in Two or More Languages from Ancient Mesopotamia and Beyond](https://journals.uc.edu/index.php/vl/article/view/5575) - Jerrold  Cooper
  + [Jackhammers and Alarm Clocks: Perceptions in Stereo](https://journals.uc.edu/index.php/vl/article/view/5576) - Daniel   Picard
  + [The Bilingual Edition in Translation Studies](https://journals.uc.edu/index.php/vl/article/view/5577) - Lance  Hewson
  + [Bilingualism in the Hebrew Text](https://journals.uc.edu/index.php/vl/article/view/5578) - Stephen  Lubell
  + [Mono versus Stereo: Bilingualism’s Double Face](https://journals.uc.edu/index.php/vl/article/view/5579) - Rainier Grutman
  + [A Case for Acadian—The Politics of Style](https://journals.uc.edu/index.php/vl/article/view/5580) - Phyllis  Wrenn
  + [Transformations in Exile: The Multilingual Exploits of Nabokov’s Pnin and Kinbote](https://journals.uc.edu/index.php/vl/article/view/5581) - Joseph Nassar
- [1992 - Vol. 26 - No. 3-4](https://journals.uc.edu/index.php/vl/issue/view/393)
  + [The Frame of Reference: Diagrams as Tools for Worldmaking](https://journals.uc.edu/index.php/vl/article/view/5566) - Sharon Helmer Poggenpohl, Dietmar R. Winkler
  + [To Show and Explain: The Information Graphics of Stevin and Comenius](https://journals.uc.edu/index.php/vl/article/view/5567) - Krysztof Lenk, Paul Kahn
  + [Sign Function and Potential of the Printed Word](https://journals.uc.edu/index.php/vl/article/view/5568) - Douglas McArthur
  + [Damned Lies. An Statistics. Otto Neurath and Soviet Propaganda in the 1930s](https://journals.uc.edu/index.php/vl/article/view/5569) - Clive Chizlett
  + [To Picture or Not to Picture: How to Decide](https://journals.uc.edu/index.php/vl/article/view/5570) - Judith E. Sims-Knight
  + [Explicit and Implicit Graphs: Changing the Frame](https://journals.uc.edu/index.php/vl/article/view/5571) - Peter Storkerson
  + [Blush and Zebrabrackets: Two Schemes for Typographical Representation of Nested Associativity](https://journals.uc.edu/index.php/vl/article/view/5572) - Michael Cohen
- [1992 - Vol. 26 - No. 1-2](https://journals.uc.edu/index.php/vl/issue/view/392)
  + [Historical Precedents, Trans-historical Strategies, and the Myth of Democratization](https://journals.uc.edu/index.php/vl/article/view/5551) - Estera   Milman
  + [Historical Design and Social Purpose: A Note on the Relationship of Fluxus to Modernism](https://journals.uc.edu/index.php/vl/article/view/5552) - Stephen C. Foster
  + [Proto-Fluxus in the United States 1959-1961: The Establishment of a Like-minded Community of Artists](https://journals.uc.edu/index.php/vl/article/view/5553) - Owen   Smith
  + [John Cage Discusses Fluxus](https://journals.uc.edu/index.php/vl/article/view/5554) - Ellsworth  Snyder
  + [Fluxus and Literature](https://journals.uc.edu/index.php/vl/article/view/5555) - Roy F. Allen
  + [Fluxacademy: From Intermedia to Interactive Education](https://journals.uc.edu/index.php/vl/article/view/5556) - Craig Saper
  + [Road Shows, Street Events, and Fluxus People; A Conversation with Alison Knowles](https://journals.uc.edu/index.php/vl/article/view/5557) - Estera Milman
  + [On Open Structures and the Crisis of Meaning, a Dialogue:](https://journals.uc.edu/index.php/vl/article/view/5558) - Stephen C. Foster, Estera Milman, Eric Anderson
  + [Two Sides of a Coin: Fluxus and the Something Else Press](https://journals.uc.edu/index.php/vl/article/view/5559) - Dick Higgins
  + [Fluxus: Global Community, Human Dimensions](https://journals.uc.edu/index.php/vl/article/view/5560) - Ken Friedman, James  Lewes
  + [Notes on SoHo and a Reminiscence](https://journals.uc.edu/index.php/vl/article/view/5561) - Hollis   Melton
  + [Circle of Friends: A Conversation with Alice Hutchins](https://journals.uc.edu/index.php/vl/article/view/5562) - Estera Milman
  + [Fluxus Fallout: New York in the Wake of the New Sensibility](https://journals.uc.edu/index.php/vl/article/view/5563) - Peter Frank
  + [FluxBase: An Interactive Art Exhibition](https://journals.uc.edu/index.php/vl/article/view/5564) - Michael Partridge, Joan Huntley
  + [Dé-Collage and Television: Wolf Vostell in New York, 1963-64](https://journals.uc.edu/index.php/vl/article/view/5565) - John G. Hanhardt, Peter Moore
- [1991 - Vol. 25 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/391)
  + [Psychology, Writing and Computers: A Review of Research](https://journals.uc.edu/index.php/vl/article/view/5549) - James Hartley
  + [The Effects of Drawing Method on the Discriminability of Characters](https://journals.uc.edu/index.php/vl/article/view/5550) - Michael L. DeKay, Jennifer J. Freyd
- [1991 - Vol. 25 - No. 2-3](https://journals.uc.edu/index.php/vl/issue/view/390)
  + [Introduction to the Artists’ Books](https://journals.uc.edu/index.php/vl/article/view/5535) - Renée Riese Hubert
  + [From Book to Anti-Book](https://journals.uc.edu/index.php/vl/article/view/5536) - Harry Polkinhorn
  + [The Book as the Trojan Horse of Art: Walter Hamady, the Perishable Press Limited and Gabberjabbs 1-6](https://journals.uc.edu/index.php/vl/article/view/5537) - Mary Lydon
  + ["Inner Tension/In Attention": Steve McCaffery’s Book Art](https://journals.uc.edu/index.php/vl/article/view/5538) - Marjorie   Perloff
  + [Deguy/Dorny  Dorny/Deguy](https://journals.uc.edu/index.php/vl/article/view/5539) - Michel Deguy
  + [Working Together: Collaboration in the Book Arts](https://journals.uc.edu/index.php/vl/article/view/5540) - Andrew   Hoyem
  + [The Computer Made Me Do It: Computers and Books](https://journals.uc.edu/index.php/vl/article/view/5541) - Paul Zelevansky
  + [Typographic Manipulation of the Poetic Text in the Early Twentieth-Century Avant-Garde](https://journals.uc.edu/index.php/vl/article/view/5542) - Johanna Drucker
  + [Covering the Text: the Object of Bookbinding](https://journals.uc.edu/index.php/vl/article/view/5543) - John Anzalone, Ruth   Copans
  + [Reading the Multimedia Book: the Case of Les Fleurs du Mal](https://journals.uc.edu/index.php/vl/article/view/5544) - Eric T.  Haskell
  + [The "Non-Book": New Dimensions in the Contemporary Artist’s Book](https://journals.uc.edu/index.php/vl/article/view/5545) - Jessica Prinz
  + [Ida Applebroog and the Book as a Performance](https://journals.uc.edu/index.php/vl/article/view/5546) - Henry M.   Sayre
  + [A Book Exhibit at the Musée Pompidou](https://journals.uc.edu/index.php/vl/article/view/5547) - Martine  Saillard
  + [The Librarian and the Artist’s Book: Notes on the Subversive Art of Cataloging](https://journals.uc.edu/index.php/vl/article/view/5548) - Timothy  Shipe
- [1991 - Vol. 25 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/389)
  + [Leading-edge Research or Lost Cause: The Search for Interscriptual Stroop Effects](https://journals.uc.edu/index.php/vl/article/view/5529) - Philippa Jane Benson
  + [Literary Assessments in Polyscriptal Societies: Chinese Character Literacy in Korea and Japan](https://journals.uc.edu/index.php/vl/article/view/5530) - R. A. Brown
  + [Copying Fluency and Orthographic Development](https://journals.uc.edu/index.php/vl/article/view/5531) - Donald R. Bear
  + [Intonation and the Comma](https://journals.uc.edu/index.php/vl/article/view/5532) - Alan   Cruttenden
  + [Spacing Printed Text to Isolate Major Phrases Improves Readability](https://journals.uc.edu/index.php/vl/article/view/5533) - Thomas G. Bever, Steven Jandreau, Rebecca Burwell, Ron Kaplan, Annie Zaenen
  + [Bastard in the Family: The Impact of Cubo-Futurist Book Art on Structural Linguistics](https://journals.uc.edu/index.php/vl/article/view/5534) - Harry Polkinhorn
- [1990 - Vol. 24 - No. 3-4](https://journals.uc.edu/index.php/vl/issue/view/388)
  + [Concerning the Beginning of Printing in 15th Century Strassburg](https://journals.uc.edu/index.php/vl/article/view/5523) - Albert Kapr
  + [Is Creativity in Alphabet Design Still Wanted?](https://journals.uc.edu/index.php/vl/article/view/5524) - Hermann Zapf
  + [Interface Design • Graphics • Language: Interpretations of Human User Interface](https://journals.uc.edu/index.php/vl/article/view/5525) - Gui Bonsiepe
  + [World Design: Broadening the Bandwidth Communication in the 90’s](https://journals.uc.edu/index.php/vl/article/view/5526) - Christopher Nemeth
  + [Communication with Visual Sound: Herbert Bayer and the Design of Type](https://journals.uc.edu/index.php/vl/article/view/5527) - Kathleen Burnett
  + [From the Bookshelves: What the User Tells the Designer](https://journals.uc.edu/index.php/vl/article/view/5528) - Paul Stiff
- [1990 - Vol. 24 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/387)
  + [Handwriting—How Much Do We Know About It?](https://journals.uc.edu/index.php/vl/article/view/5516) - Rosemary Sassoon
  + [Who on Earth Invented the Alphabet?](https://journals.uc.edu/index.php/vl/article/view/5517) - John Sassoon
  + [Questioned Documents: The Human Trace as a Body Flow](https://journals.uc.edu/index.php/vl/article/view/5518) - Marie-Jeanne Sedeyn
  + [Preliminary Thoughts on Nomenclature for Teachers of Handwriting](https://journals.uc.edu/index.php/vl/article/view/5519) - Michael Twyman, Susan Walker
  + [Anatomy of the Hand](https://journals.uc.edu/index.php/vl/article/view/5520) - Michael Patkin
  + [Writer’s Cramp](https://journals.uc.edu/index.php/vl/article/view/5521) - Rosemary Sassoon
  + [Graphic Skills as a Diagnostic Tool For Working with the Elderly](https://journals.uc.edu/index.php/vl/article/view/5522) - Kjerstin Ericsson
- [1990 - Vol. 24 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/386)
  + [An Overview of Mesoamerica](https://journals.uc.edu/index.php/vl/article/view/5510) - F. Kent Reilly III, Brian Stross
  + [Cosmos and Rulership: The Function of Olmec-style Symbols in Formative Period Mesoamerica](https://journals.uc.edu/index.php/vl/article/view/5511) - F. Kent Reilly III
  + [Mesoamerican Writing at the Crossroads: The Late Formative](https://journals.uc.edu/index.php/vl/article/view/5512) - Brian Stross
  + [Deciphering Maya Hieroglyphic Writing: The State of the Art](https://journals.uc.edu/index.php/vl/article/view/5513) - Virginia M. Fields
  + [Pre-Hispanic Pictoral Communication: The Codex System of the Mixtec of Oaxaca, Mexico](https://journals.uc.edu/index.php/vl/article/view/5514) - Nancy P. Troike
  + [Evolutionary Trends in Mesoamerican Hieroglyphic Writing](https://journals.uc.edu/index.php/vl/article/view/5515) - John S. Justeson, Peter Mathews
- [1989 - Vol. 23 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/385)
  + [The Effects of Italic Handwriting on Legibility](https://journals.uc.edu/index.php/vl/article/view/5506) - Charles Lehman, Carolyn  Moilanen
  + [Why Beginning Reading Must Be Word-By-Word: Disfluent Oral Reading and Orthographic Development](https://journals.uc.edu/index.php/vl/article/view/5507) - Donald R. Bear
  + [The Tales Typography Tells](https://journals.uc.edu/index.php/vl/article/view/5508) - Edward A. Riedinger
  + [Image and Narritivity: Robbe-Grillet’s La Belle Captive](https://journals.uc.edu/index.php/vl/article/view/5509) - Emma Kadalenos
- [1989 - Vol. 23 - No. 2-3](https://journals.uc.edu/index.php/vl/issue/view/384)
  + [By Way of Introduction: Inscriptions as Subversion](https://journals.uc.edu/index.php/vl/article/view/5497) - Claude Gandelman
  + [Some Oriental Pseudo-Inscriptions in Renaissance Art](https://journals.uc.edu/index.php/vl/article/view/5498) - Moshe Barasch
  + [The Order of Words and the Order of Things in Painting](https://journals.uc.edu/index.php/vl/article/view/5499) - Louis Martin
  + [The Contra-Diction of Design: Blake’s Illustrations to Gray’s "Ode on the Death of a Favourite Cat"](https://journals.uc.edu/index.php/vl/article/view/5500) - Mark Lussier
  + [Magritte’s Words and Images](https://journals.uc.edu/index.php/vl/article/view/5501) - George Roque
  + [The Books of Fernand Léger: Illustration and Inscription](https://journals.uc.edu/index.php/vl/article/view/5502) - Renée Riese Hubert
  + [Jasper Johns’ Painted Words](https://journals.uc.edu/index.php/vl/article/view/5503) - Esther Levinger
  + [On the Verbal Art of a Modern Painter: the Work of Jules Kirschenbaum](https://journals.uc.edu/index.php/vl/article/view/5504) - Claude Gandelman
  + [Magritte’s Captivity in Robbe-Grillet’s La Belle Captive: The Subjugation of the Image by the Word](https://journals.uc.edu/index.php/vl/article/view/5505) - Leslie Ortquist
- [1989 - Vol. 23 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/383)
  + [The Visual Poem in the Eighteenth Century](https://journals.uc.edu/index.php/vl/article/view/5491) - Richard Bradford
  + [Mallarmé and Apollinaire: The Unpunctured Text](https://journals.uc.edu/index.php/vl/article/view/5492) - R.A. York
  + [Words-in-Freedom and the Oral Tradition](https://journals.uc.edu/index.php/vl/article/view/5493) - Michael Webster
  + [Visual Form in Free Verse](https://journals.uc.edu/index.php/vl/article/view/5494) - Eleanor Berry
  + [Which Poem am I Reading?](https://journals.uc.edu/index.php/vl/article/view/5495) - E.A. Markham
  + [Soundings Along the Lines](https://journals.uc.edu/index.php/vl/article/view/5496) - Andrew Waterman
- [1988 - Vol. 22 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/382)
  + [The Imperialism of Syntax](https://journals.uc.edu/index.php/vl/article/view/5485) - -- Laiwan
  + [Handbook For a Theory Hobby: The Hobby-Horse is the Sawhorse of Theory](https://journals.uc.edu/index.php/vl/article/view/5486) - Gregory L. Ulmer
  + [Decoder Process](https://journals.uc.edu/index.php/vl/article/view/5487) - Bonnie Sparling
  + [Condensed Article](https://journals.uc.edu/index.php/vl/article/view/5488) - Avital Ronell
  + [A Writing of the Real](https://journals.uc.edu/index.php/vl/article/view/5489) - Ellie Ragland-Sullivan
  + [The ABC of Visual Theory](https://journals.uc.edu/index.php/vl/article/view/5490) - Robert B. Ray
- [1988 - Vol. 22 - No. 2-3](https://journals.uc.edu/index.php/vl/issue/view/381)
  + [Literacy Literacy](https://journals.uc.edu/index.php/vl/article/view/5478) - Eugene R. Kintgen
  + [Speech and Writing in Poetry and Its Criticism](https://journals.uc.edu/index.php/vl/article/view/5479) - Richard Bradford
  + [Verbal and Visual Translation of Mayakovsky’s and Lissitsky’s For Reading Out Loud](https://journals.uc.edu/index.php/vl/article/view/5480) - Martha Scotford Lange
  + [For Reading Out Loud in Context](https://journals.uc.edu/index.php/vl/article/view/5481) - Szymon Bojko, Krzysztof  Lenk
  + [Metadiscourse and the Recall of Modality Markers](https://journals.uc.edu/index.php/vl/article/view/5482) - William J. Vande, Allen  Shoemaker
  + [Typographic Cues As an Aid to Learning from Textbooks](https://journals.uc.edu/index.php/vl/article/view/5483) - Karen M. Garofalo
  + [Meditation: Visual Transition as a Bridge Between Form and Meaning](https://journals.uc.edu/index.php/vl/article/view/5484) - Todd Cavalier
- [1988 - Vol. 22 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/380)
  + [Visible Language in Speech Perception: Lipreading and Reading](https://journals.uc.edu/index.php/vl/article/view/5472) - Dominic W. Massaro, Michael M. Cohen
  + [Tracing Lip Movements: Making Speech Visible](https://journals.uc.edu/index.php/vl/article/view/5473) - Ruth Campbell
  + [Cross-Modal Effects in Repetition Priming: A Comparison of Lipread, Graphic, and Heard Stimuli](https://journals.uc.edu/index.php/vl/article/view/5474) - Barbara Dodd, Michael Oerlemans, Ray Robinson
  + [Perception of Facial Movements in Early Infancy: Some Reflections in Relation to Speech Perception](https://journals.uc.edu/index.php/vl/article/view/5475) - Annie Vinter
  + [Reading the Speech of Digital Lips: Motives and Methods for Audio-Visual Speech Synthesis](https://journals.uc.edu/index.php/vl/article/view/5476) - Darryl Storey, Martin Roberts
  + [Speaking from Two Sides of the Mouth](https://journals.uc.edu/index.php/vl/article/view/5477) - Roger E. Graves, Susan M. Potter
- [1987 - Vol. 21 - No. 3-4](https://journals.uc.edu/index.php/vl/issue/view/379)
  + [The Prerequisite Text](https://journals.uc.edu/index.php/vl/article/view/5465) - Stephen C. Foster
  + [The Text and the Myth of the Avant-Garde](https://journals.uc.edu/index.php/vl/article/view/5466) - Estera Milman
  + [The Tradition of the Avant-Garde](https://journals.uc.edu/index.php/vl/article/view/5467) - Roy Allen
  + [Between Text and Audience: a Path to the Future](https://journals.uc.edu/index.php/vl/article/view/5468) - Allen Greenberg
  + [The Text and the Coming of Age of the Avant-Garde in Germany](https://journals.uc.edu/index.php/vl/article/view/5469) - Timothy O. Benson
  + [Berlin Dada](https://journals.uc.edu/index.php/vl/article/view/5470) - Peter Guenther
  + [The Dadaist Text: Politics, Aesthetics, and Alternative Cultures?](https://journals.uc.edu/index.php/vl/article/view/5471) - Rainer Rumold
- [1987 - Vol. 21 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/378)
  + [Readers Writing: The Curriculum of the Writing Schools of Eighteenth Century Boston](https://journals.uc.edu/index.php/vl/article/view/5460) - Jennifer E. Monaghan
  + [American Spelling Instruction: Retrospect and Prospect](https://journals.uc.edu/index.php/vl/article/view/5461) - Richard E. Hodges
  + [Writing as Praxis 1900-1959](https://journals.uc.edu/index.php/vl/article/view/5462) - Barbara von Bracht Donsky
  + [Fourth Grade Writing Achievement and Instruction, 1974-1984: NAEP’s Report Card](https://journals.uc.edu/index.php/vl/article/view/5463) - Anne Campbell
  + [Young Children Then and Now: Recent Research on Emergent Literacy](https://journals.uc.edu/index.php/vl/article/view/5464) - Nancy A. Mavrogenes
- [1987 - Vol. 21 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/377)
  + [Graphic Collisions: Languages in Contact](https://journals.uc.edu/index.php/vl/article/view/5454) - Richard Hodgson, Ralph Sarkonak
  + [Bilingual Typography](https://journals.uc.edu/index.php/vl/article/view/5455) - Alistair Crawford
  + [Foreign Loanwords in Dutch: Integration and Adaptation](https://journals.uc.edu/index.php/vl/article/view/5456) - Henry Schogt
  + [The Roman Alphabet in the Japanese Writing System](https://journals.uc.edu/index.php/vl/article/view/5457) - Bernard Saint-Jacques
  + [Ortho- and Morpho-graphic Transcoding of Acadian "Franglais"](https://journals.uc.edu/index.php/vl/article/view/5458) - Phyllis Wrenn
  + [Converging Languages in a World of Conflicts: Code-switching in Chicano Poetry](https://journals.uc.edu/index.php/vl/article/view/5459) - Lauro Flores
- [1986 - Vol. 20 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/376)
  + [Instructional Text: The Transition from Page to Screen](https://journals.uc.edu/index.php/vl/article/view/5448) - Stephen T. Kerr
  + [Italian Sixteenth-Century Italian Writing Books and the Scribal Reality of Verona](https://journals.uc.edu/index.php/vl/article/view/5449) - Richard W. Clement
  + [Variations in Spelling and the Special Case of Colloquial Contractions](https://journals.uc.edu/index.php/vl/article/view/5450) - Rose-Marie Weber
  + [Proximité du Murmure: Dupin and Ubac Collaboratel](https://journals.uc.edu/index.php/vl/article/view/5451) - Maryanne De Julio
  + [Differences between Good and Poor Spellers in Reading Style and Short-Term Memory](https://journals.uc.edu/index.php/vl/article/view/5452) - Jeanne Ellis Ormond
  + [Line Lengths and Starch Scores](https://journals.uc.edu/index.php/vl/article/view/5453) - Sandra E. Moriarty
- [1986 - Vol. 20 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/375)
  + [Tokens: Facts and Interpretations](https://journals.uc.edu/index.php/vl/article/view/5441) - Denise Schmandt-Besserat
  + [Alphabetic Literacy and Brain Processes](https://journals.uc.edu/index.php/vl/article/view/5442) - Derrick de Kerckhove
  + [Texts, Readers, and Enacted Narratives](https://journals.uc.edu/index.php/vl/article/view/5443) - Brian Stock
  + [Interpreting Texts and Interpreting Nature: The Effects of Literacy and Epistemology](https://journals.uc.edu/index.php/vl/article/view/5444) - David Olson
  + [Writing, Religion, and Revolt in Bahia](https://journals.uc.edu/index.php/vl/article/view/5445) - Jack Goody
  + [Of Stone, Books, and Freedom](https://journals.uc.edu/index.php/vl/article/view/5446) - Vincent de Norcia
  + [Innis, McLuhan, and Marx](https://journals.uc.edu/index.php/vl/article/view/5447) - Carolyn Marvin
- [1986 - Vol. 20 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/374)
  + [Analyzing the Various Approaches of Plain Language Laws](https://journals.uc.edu/index.php/vl/article/view/5434) - Erwin R. Steinberg, Betsy A. Bowen, Thomas M. Duffy
  + [Plain English: The Remaining Problems](https://journals.uc.edu/index.php/vl/article/view/5435) - Joseph M. Williams
  + [Prescriptive Linguistics and Plain English: The Case of "Whiz-deletions"](https://journals.uc.edu/index.php/vl/article/view/5436) - Thomas N. Huckin, Elizabeth H. Curtin, Debra Graham
  + [Computers for Composition: A Stage Model Approach to Helping](https://journals.uc.edu/index.php/vl/article/view/5437) - Lance A. Miller
  + [Plain English on the Plant Floor](https://journals.uc.edu/index.php/vl/article/view/5438) - Barry Jereb
  + [A Program for Improving Documentation](https://journals.uc.edu/index.php/vl/article/view/5439) - Erwin R. Steinberg
  + [Writing Backwards: The Use of Visual Models in Writing](https://journals.uc.edu/index.php/vl/article/view/5440) - G. Reed Agnew
- [1986 - Vol. 20 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/373)
  + [The Figured Poem: Towards a Definition of Genre](https://journals.uc.edu/index.php/vl/article/view/5426) - Ulrich Ernst
  + [The Corpus of British and Other English-Language Pattern Poetry](https://journals.uc.edu/index.php/vl/article/view/5427) - Dick Higgins
  + [Reading Paths in Spanish and Portuguese Baroque Labyrinths](https://journals.uc.edu/index.php/vl/article/view/5428) - Ana Hatherly
  + [The Labyrinth Poem](https://journals.uc.edu/index.php/vl/article/view/5429) - Piotr Rypson
  + [Chinese Patterned Texts](https://journals.uc.edu/index.php/vl/article/view/5430) - Herbert Franke
  + [Sanskrit Citrakavyas and the Western Pattern Poem: A Critical Appraisal](https://journals.uc.edu/index.php/vl/article/view/5431) - Kalanath Jha
  + [Pastoral Typography: Sigmund von Birken and the "Picture-Rhymes" of Johann Helwig](https://journals.uc.edu/index.php/vl/article/view/5432) - Jeremy Adler
  + [Georg Weber’s Lebens-Früchte (1649)](https://journals.uc.edu/index.php/vl/article/view/5433) - Karl F. Otto, Jr.
- [1985 - Vol. 19 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/372)
  + [Visible Language in Contemporary Culture](https://journals.uc.edu/index.php/vl/article/view/5419) - Laurie Edson
  + [Poetry, Portrait, Poetrait](https://journals.uc.edu/index.php/vl/article/view/5420) - Steven Winspur
  + [A Visionary Book: Charles Nodier's L'Histoire du Roi de Boheme et de ses sept chateaux](https://journals.uc.edu/index.php/vl/article/view/5421) - Anne-Marie Christin
  + [The "Logical Status" of Words in Painting](https://journals.uc.edu/index.php/vl/article/view/5422) - Carol Plyley   James
  + [Samuel Beckett: Color, Letter, and Line](https://journals.uc.edu/index.php/vl/article/view/5423) - Tom Conley
  + [Reflections on the Illustrated Book](https://journals.uc.edu/index.php/vl/article/view/5424) - Renee Hubert
  + [Press Art: Poets and their Printing Machines](https://journals.uc.edu/index.php/vl/article/view/5425) - Judith Preckshot
- [1985 - Vol. 19 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/371)
  + [Adult Literacy and Technology](https://journals.uc.edu/index.php/vl/article/view/5415) - Raymond S.   Nickerson
  + [The New Imperative in Literary Criticism](https://journals.uc.edu/index.php/vl/article/view/5416) - W. John Harker
  + [The Inner Functioning of Words: Iconicity in Poetic Language](https://journals.uc.edu/index.php/vl/article/view/5417) - Paschal C.   Viglionese
  + [The Mind's Eye and the CRT Terminal: Toward a Diagrammatic Interface](https://journals.uc.edu/index.php/vl/article/view/5418) - Jeff Nickerson
- [1985 - Vol. 19 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/370)
  + [Computer Graphics: What Do They Mean and How Do They Fit?](https://journals.uc.edu/index.php/vl/article/view/5409) - Sharon   Poggenpohl
  + [Scripting Graphic With Graphics](https://journals.uc.edu/index.php/vl/article/view/5410) - Dorothy Shamonsky
  + [Spatial Context as an Aid to Page Layout](https://journals.uc.edu/index.php/vl/article/view/5411) - Morissa M. Rubin
  + [Dynamic Information Display](https://journals.uc.edu/index.php/vl/article/view/5412) - Yoshiki Nishimura, Keiichi  Sato
  + [Computers in Design Education](https://journals.uc.edu/index.php/vl/article/view/5413) - Leif Allmendinger, Mihai  Nadin
  + [Graphic Design: Towards Digital Applications](https://journals.uc.edu/index.php/vl/article/view/5414) - Mary Jones
- [1985 - Vol. 19 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/369)
  + [A Turning Point in Type Design](https://journals.uc.edu/index.php/vl/article/view/5399) - John Dreyfus
  + [Future Tendencies in Type Design: The Scientific Approach to Letterforms](https://journals.uc.edu/index.php/vl/article/view/5400) - Hermann Zapf
  + [Lessons Learned from Metafont](https://journals.uc.edu/index.php/vl/article/view/5401) - Donald Knuth
  + [Stonecuttings from David Kindersley's Workshop](https://journals.uc.edu/index.php/vl/article/view/5402) - Linda Lopes Cardozo
  + [The Transylvanian Phoenix: The Kis-Janson Types in the Digital Era](https://journals.uc.edu/index.php/vl/article/view/5403) - Jack Stauffacher
  + [Galliard: A Modern Revival of the Types of Robert Gran jon](https://journals.uc.edu/index.php/vl/article/view/5404) - Matthew Carter
  + [Punchcutting Demonstration](https://journals.uc.edu/index.php/vl/article/view/5405) - Henk Drost
  + [Mould Making, Matrix Fitting and Hand Casting](https://journals.uc.edu/index.php/vl/article/view/5406) - Stan Nelson
  + [Fundamental Research Methods and Form Innovation in Type Design Compared to Technological Developments in Type Production](https://journals.uc.edu/index.php/vl/article/view/5407) - André Gürtler, Christian Mengelt
  + [The State of the Art in Typeface Design Protection](https://journals.uc.edu/index.php/vl/article/view/5408) - Edward Gottschall
- [1984 - Vol. 18 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/368)
  + [Reading and Working Memory](https://journals.uc.edu/index.php/vl/article/view/5391) - Alan Baddeley
  + [Reading Ability and Orthographic Structure](https://journals.uc.edu/index.php/vl/article/view/5392) - Dominic W. Massaro
  + [Is Activation of Different Codes Related to Age and Stimulus Material?](https://journals.uc.edu/index.php/vl/article/view/5393) - F. Simion, B. Benelli, F. Tarantini
  + [Morphological Structures and Lexical Access](https://journals.uc.edu/index.php/vl/article/view/5394) - C. Burani, D. Salmaso, A. Caramazza
  + [Lexical Knowledge and Word Recognition: Children’s Reading of Function Words](https://journals.uc.edu/index.php/vl/article/view/5395) - Giavanni B. Flores d'Arcais
  + [Structural vs. Semantic Coding in Reading of Isolated Works by Deaf Children](https://journals.uc.edu/index.php/vl/article/view/5396) - A. Cavedon, C. Cornoldi, R. DeBeni
  + [Surface Dyslexia and Developmental Disorders of Reading](https://journals.uc.edu/index.php/vl/article/view/5397) - J. Masterson
  + [Reading and Remembering](https://journals.uc.edu/index.php/vl/article/view/5398) - Camille L. Z. Blachowicz
- [1984 - Vol. 18 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/367)
  + [Graphological Structure of Japanese](https://journals.uc.edu/index.php/vl/article/view/5386) - A.E. Backhouse
  + [The Adoption of Punctuation in Japanese Script](https://journals.uc.edu/index.php/vl/article/view/5387) - Nannette Twine
  + [Japanese Orthography in the Computer Age](https://journals.uc.edu/index.php/vl/article/view/5388) - J. Marshall Unger
  + [Japanese Braille](https://journals.uc.edu/index.php/vl/article/view/5389) - J. Marshall Unger
  + [Japanese Script since 1900](https://journals.uc.edu/index.php/vl/article/view/5390) - Chris Seeley
- [1984 - Vol. 18 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/366)
  + [How Typewriters Changed Correspondence](https://journals.uc.edu/index.php/vl/article/view/5380) - Sue Walker
  + [Computer Mediated Communication as a Force in Language Change](https://journals.uc.edu/index.php/vl/article/view/5381) - Naomi S. Baron
  + [Reading Library Catalogues and Indexes](https://journals.uc.edu/index.php/vl/article/view/5382) - Philip Bryant
  + [Moving Tables from Paper to Screen](https://journals.uc.edu/index.php/vl/article/view/5383) - Pat Norrish
  + [Design and Presentation of Computer Human Factors Journal on the BLEND System](https://journals.uc.edu/index.php/vl/article/view/5384) - David J. Pullinger
  + [Investigating Referee's Requirements in an Electronic Medium](https://journals.uc.edu/index.php/vl/article/view/5385) - P. Wright, A. Lickorish
- [1984 - Vol. 18 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/365)
  + [Reading Research in Metalinguistic Awareness](https://journals.uc.edu/index.php/vl/article/view/5375) - David Yaden
  + [Before Numerals](https://journals.uc.edu/index.php/vl/article/view/5376) - Denise Schmandt-Besserat
  + [Effects of Chunking and Line Length on Reading Efficiency](https://journals.uc.edu/index.php/vl/article/view/5377) - Stacey Keenan
  + [The Visual Editing of Texts](https://journals.uc.edu/index.php/vl/article/view/5378) - Fernand Baudin
  + [Hebrew Micrography: 1,000 Years of Art in Script](https://journals.uc.edu/index.php/vl/article/view/5379) - Leila Avrin
- [1983 - Vol. 17 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/364)
  + [Handwriting in English Education](https://journals.uc.edu/index.php/vl/article/view/5368) - Prue Wallis Myers
  + [Information Distribution in Chinese Characters](https://journals.uc.edu/index.php/vl/article/view/5369) - Yao-Chung Tsao, Tsai-Guey Wang
  + [Behind the Slash](https://journals.uc.edu/index.php/vl/article/view/5370) - Rose-Marie Weber
  + [Graphical Abstractions of Technical Documents](https://journals.uc.edu/index.php/vl/article/view/5371) - Gary Perlman, Thomas D. Erickson
  + [Hebrew Hieroglyphics](https://journals.uc.edu/index.php/vl/article/view/5372) - Zev Bar-Lev
  + [Significance of Word Length](https://journals.uc.edu/index.php/vl/article/view/5373) - Patrick Groff
  + [The Future for Books in the Electronic Era](https://journals.uc.edu/index.php/vl/article/view/5374) - J. Robert Moskin
- [1983 - Vol. 17 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/363)
  + [Letterisme: A Point of View](https://journals.uc.edu/index.php/vl/article/view/5360) - Stephen C. Foster
  + [Chronology: Jean-Paul Curtay](https://journals.uc.edu/index.php/vl/article/view/5361) - Stephen C. Foster
  + [Letterisme - A Stream That That Runs Own Course](https://journals.uc.edu/index.php/vl/article/view/5362) - David W. Seaman
  + [Super-Writing 1983 -America 1683](https://journals.uc.edu/index.php/vl/article/view/5363) - Jean-Paul Curtay
  + [Approaching Letterist Cinema](https://journals.uc.edu/index.php/vl/article/view/5364) - Frederique Devaux
  + [The Limitations of Letterisme: An Interview with Henri Chopin](https://journals.uc.edu/index.php/vl/article/view/5365) - Nicholas Zurbrugg
  + [Selected Theoretical Texts from Letterists](https://journals.uc.edu/index.php/vl/article/view/5366) - David W. Seaman
  + [Researching Letterisme and Bibliography](https://journals.uc.edu/index.php/vl/article/view/5367) - Pietro Ferrua
- [1983 - Vol. 17 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/362)
  + [The Relation of the Whole to the Part in Interpretation Theory and in the Composing Process](https://journals.uc.edu/index.php/vl/article/view/5355) - James L. Kinneavy
  + [The Hermeneutic Phenomenon and the Authenticity of Discourse](https://journals.uc.edu/index.php/vl/article/view/5356) - Michael J. Hyde
  + [Reading, Writing: Radix](https://journals.uc.edu/index.php/vl/article/view/5357) - Charles R. Kline, Jr., Roland K. Huff
  + [Topical Structure and Writing Quality: Some Possible Text-Based Explanations of Readers' Judgments o](https://journals.uc.edu/index.php/vl/article/view/5358) - Stephen P. Witte
  + [Hermeneutics as Criticism](https://journals.uc.edu/index.php/vl/article/view/5359) - Adrian Marino
- [1983 - Vol. 17 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/361)
  + ["What parts of your work give you the most trouble?"](https://journals.uc.edu/index.php/vl/article/view/5354) - Gunnlauger S. E. Briem
- [1982 - Vol. 16 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/360)
  + [Meta-Font, Metamathematics, and Metaphysics: Comments on Donald Knuth's Article (16:1)](https://journals.uc.edu/index.php/vl/article/view/5350) - Douglas R. Hofstadter
  + [Interrelationships: Drawing and Early Writing](https://journals.uc.edu/index.php/vl/article/view/5351) - Anne Dyson
  + [Color in Viewdata Displays](https://journals.uc.edu/index.php/vl/article/view/5352) - Jeremy J. Foster, Margaret Bruce
  + [The Wooly Jumper: Typographical Problems of Concurrency in Information Display](https://journals.uc.edu/index.php/vl/article/view/5353) - T.R.G. Green, S.J. Payne
- [1982 - Vol. 16 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/359)
  + [Difficulties with Symbolism: Synonymy and Homonymy](https://journals.uc.edu/index.php/vl/article/view/5341) - J. Adda
  + [Emotional Responses to Symbolism](https://journals.uc.edu/index.php/vl/article/view/5342) - Laurie G. Buxton
  + [Mathematical Language and Problem Solving](https://journals.uc.edu/index.php/vl/article/view/5343) - Gerald A. Goldin
  + [Symbols, Icons, and Mathematical Understanding](https://journals.uc.edu/index.php/vl/article/view/5344) - William Higginson
  + [Towards Recording](https://journals.uc.edu/index.php/vl/article/view/5345) - Nick James, John Mason
  + [Mental Images and Arithmetical Symbols](https://journals.uc.edu/index.php/vl/article/view/5346) - L. Clark Lay
  + [Language Acquisition through Math Symbolism](https://journals.uc.edu/index.php/vl/article/view/5347) - Francis Lowenthal
  + [Communicating Mathematics: Surface Structures and Deep Structures](https://journals.uc.edu/index.php/vl/article/view/5348) - Richard R. Skemp
  + [Mathematical Symbolism](https://journals.uc.edu/index.php/vl/article/view/5349) - Derek Woodrow
- [1982 - Vol. 16 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/358)
  + [The Origins of the Present-day Chasm between Adult Literacy Needs and School Literacy Instruction](https://journals.uc.edu/index.php/vl/article/view/5336) - Richard L. Venezky
  + [Literacy for Empowerment and Social Change](https://journals.uc.edu/index.php/vl/article/view/5337) - Carman St. John Hunter
  + [What is Said and What is Meant in Speech and Writing](https://journals.uc.edu/index.php/vl/article/view/5338) - David R. Olson
  + [Eye Dialect as a Problem in Graphics](https://journals.uc.edu/index.php/vl/article/view/5339) - Paul H. Bowdre, Jr.
  + [Mr. Dooley's Brogue:The Literary Dialect of Finley Peter Dunne](https://journals.uc.edu/index.php/vl/article/view/5340) - Clyde Thogmartin
- [1982 - Vol. 16 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/357)
  + [The Concept of a Meta-Font](https://journals.uc.edu/index.php/vl/article/view/5329) - Donald E. Knuth
  + [English Spelling and Phonemic Representation](https://journals.uc.edu/index.php/vl/article/view/5330) - Royal Skousen
  + [Costume and Name in Mesoamerica](https://journals.uc.edu/index.php/vl/article/view/5331) - David H. Kelley
  + [Biblica: Designing a Typeface for the Bible](https://journals.uc.edu/index.php/vl/article/view/5332) - Kurt Weidemann
  + [Text Displayby "Saccadic Scrolling"](https://journals.uc.edu/index.php/vl/article/view/5333) - Andrew Sekey, Jerome Tietz
  + [Literacy for What?](https://journals.uc.edu/index.php/vl/article/view/5334) - Maxine Greene
  + [Improving the Legibility of Textbooks: Effects of Wording and Typographic Design](https://journals.uc.edu/index.php/vl/article/view/5335) - Dirk Wendt
- [1981 - Vol. 15 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/356)
  + [From Tokens to Tablets: A Re-evaluation of the So-called "Numerical Tablets"](https://journals.uc.edu/index.php/vl/article/view/5324) - Denise Schmandt-Besserat
  + [Construction and Implementation of the Cuneiform System](https://journals.uc.edu/index.php/vl/article/view/5325) - M.W. Green
  + [Visible Sentences in Cuneiform Hittite](https://journals.uc.edu/index.php/vl/article/view/5326) - Carol Justus
  + [The Temple Scribe in Chaldean Uruk](https://journals.uc.edu/index.php/vl/article/view/5327) - Ronald H. Sack
  + [Three Problems in the History of Cuneiform Writing: Origins, Direction of Script, and Literacy](https://journals.uc.edu/index.php/vl/article/view/5328) - Marvin A. Powell
- [1981 - Vol. 15 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/355)
  + [Children's Word Recognition in Prose Context](https://journals.uc.edu/index.php/vl/article/view/5321) - Susan F. Ehrlich
  + [Disorders of Reading and Their Implications for Models of Normal Reading](https://journals.uc.edu/index.php/vl/article/view/5322) - Max Coltheart
  + [Word Processing in Reading: A Commentary on the Papers](https://journals.uc.edu/index.php/vl/article/view/5323) - Arnold D. Well, Alexander Pollatsek
- [1981 - Vol. 15 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/354)
  + [Visual Cues in Word Recognition and Reading: Introduction](https://journals.uc.edu/index.php/vl/article/view/5317) - Keith Rayner
  + [Visual Factors and Eye Movements in Reading](https://journals.uc.edu/index.php/vl/article/view/5318) - Robert E. Morrison, Albrecht-Werner Inhoff
  + [Visual Components of the Reading Process](https://journals.uc.edu/index.php/vl/article/view/5319) - Ralph Norman Haber, Lyn R. Haber
  + [Toward a Computational Theory of Early Visual Processing in Reading](https://journals.uc.edu/index.php/vl/article/view/5320) - Michael Brady
- [1981 - Vol. 15 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/353)
  + [Typography without Words](https://journals.uc.edu/index.php/vl/article/view/5310) - Michael Twyman
  + [Layout and Wording Changes for Instructional Text](https://journals.uc.edu/index.php/vl/article/view/5311) - James Hartley, Mark Trueman
  + [Multi-level Writing in Theory and Practice](https://journals.uc.edu/index.php/vl/article/view/5312) - Don L. Jewett
  + [Typographical and Spatial Cues that Facilitate Learning from Textbooks](https://journals.uc.edu/index.php/vl/article/view/5313) - Wayne L. Shebilske, John A. Rotondo
  + [Information Mapping: a Description and Rationale](https://journals.uc.edu/index.php/vl/article/view/5314) - David Scott Jonassen
  + [Patterned Note-Taking: an Evaluation](https://journals.uc.edu/index.php/vl/article/view/5315) - Linda S. Norton
  + [The Uses of Space in Music Notation](https://journals.uc.edu/index.php/vl/article/view/5316) - John Sloboda
- [1980 - Vol. 14 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/352)
  + [Arts, Crafts, Gifts, and Knacks](https://journals.uc.edu/index.php/vl/article/view/5303) - Richard E. Young
  + [Conformity and Commitment in Writing](https://journals.uc.edu/index.php/vl/article/view/5304) - Peter C. Wason
  + [The Effects of Conflicting Goals on Writing](https://journals.uc.edu/index.php/vl/article/view/5305) - David Galbriath
  + [Writing as Conversation and Letter to a Novelist](https://journals.uc.edu/index.php/vl/article/view/5306) - Richard Stack
  + [Mixing Levels of Revision](https://journals.uc.edu/index.php/vl/article/view/5307) - David Lowenthal
  + [Writing as Problem Solving](https://journals.uc.edu/index.php/vl/article/view/5308) - John R. Hayes, Linda S. Flower
  + [Writing as a Cognitive Activity](https://journals.uc.edu/index.php/vl/article/view/5309) - Robert J.  Bracewell
- [1980 - Vol. 14 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/351)
  + [Visible Language: Freud's Imprint](https://journals.uc.edu/index.php/vl/article/view/5296) - Mary Lydon
  + [Lex Icon: Freud and Rimbaud](https://journals.uc.edu/index.php/vl/article/view/5297) - Andrew J. McKenna
  + [Killer Bees: An Ontology in Abeyance](https://journals.uc.edu/index.php/vl/article/view/5298) - Sanford S. Ames
  + [Robbe-Grillet on Target Or Interrogation by the Numbers](https://journals.uc.edu/index.php/vl/article/view/5299) - George H. Bauer
  + [Freud's Invisible Chiasmus](https://journals.uc.edu/index.php/vl/article/view/5300) - Jane Gallop
  + [Defoe's Daydream: Becoming Moll Flanders](https://journals.uc.edu/index.php/vl/article/view/5301) - Susanna Bartman
  + [Franked Letters: Crossing the Bar](https://journals.uc.edu/index.php/vl/article/view/5302) - Eleanor Honig Skoller
- [1980 - Vol. 14 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/350)
  + [Writing and the Concept of Law in Ancient Greece](https://journals.uc.edu/index.php/vl/article/view/5291) - Jon Stratton
  + [In Defense of Conservatism in English Orthography](https://journals.uc.edu/index.php/vl/article/view/5292) - Phillip T. Smith
  + [A Syllable Frequency Count](https://journals.uc.edu/index.php/vl/article/view/5293) - Edward Fry, Elizabeth Sakiey, Albert Goss, Barry Loigman
  + [Strategy and Tactics in the Design of Forms](https://journals.uc.edu/index.php/vl/article/view/5294) - Patricia Wright
  + ["Beware of the Scribes"](https://journals.uc.edu/index.php/vl/article/view/5295) - Humphrey Lyttelton
- [1980 - Vol. 14 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/349)
  + [The Distribution of Visual Information in the Vertical Dimension of Roman and Hebrew Letters](https://journals.uc.edu/index.php/vl/article/view/5286) - Joseph Shimron, David Navon
  + [Figure/ground, Brightness Contrast, and Reading Disabilities](https://journals.uc.edu/index.php/vl/article/view/5287) - Olive Meares
  + [Visible Language Policy - Bilingualism and Multilingualism on Postage Stamps](https://journals.uc.edu/index.php/vl/article/view/5288) - Richard E. Wood
  + [Signs in Ancient Egypt: Another Look at the Relations of Figure to Hieroglyph](https://journals.uc.edu/index.php/vl/article/view/5289) - Jean M. James
  + [Spatial Cues in Text: Some Comments on the Paper by Frase & Schwartz](https://journals.uc.edu/index.php/vl/article/view/5290) - James Hartley
- [1979 - Vol. 13 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/348)
  + [A Practice in Search of Theory](https://journals.uc.edu/index.php/vl/article/view/5281) - Sharon Poggenpohl
  + [Visual Differential Theory](https://journals.uc.edu/index.php/vl/article/view/5282) - Robert Manning
  + [A Language of Form: Two Dimensional Isometric System](https://journals.uc.edu/index.php/vl/article/view/5283) - David Stuhr
  + [Semiotics and Graphic Design Education](https://journals.uc.edu/index.php/vl/article/view/5284) - Thomas Ockerse, Hans C. van Dijk
  + [The Graphic Information Research Unit](https://journals.uc.edu/index.php/vl/article/view/5285) - Linda Reynolds
- [1979 - Vol. 13 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/347)
  + [Facility of Handwriting Using Different Movements](https://journals.uc.edu/index.php/vl/article/view/5271) - R.S. Woodworth
  + [Computer-Aided Analysis of Handwriting Movements](https://journals.uc.edu/index.php/vl/article/view/5272) - Hans-Leo H. M. Tuelings, Arnold J. W. M. Thomassen
  + [A Computer to Check Signatures](https://journals.uc.edu/index.php/vl/article/view/5273) - R.S. Watson, P.J. Pobgee
  + [Handwriting Classification of Forsenic Science](https://journals.uc.edu/index.php/vl/article/view/5274) - Michael Ansell
  + [A Competence Model of Handwriting](https://journals.uc.edu/index.php/vl/article/view/5275) - John Hollerbach
  + [Slips of the Pen](https://journals.uc.edu/index.php/vl/article/view/5276) - Andrew W. Ellis
  + [Variability of Handwritten Characters](https://journals.uc.edu/index.php/vl/article/view/5277) - Alan M. Wing
  + [Directional Preference in Writing](https://journals.uc.edu/index.php/vl/article/view/5278) - Arnold J. W. M. Thomassen, Hans-Leo H.M Tuelings
  + [Children's Copying Performances](https://journals.uc.edu/index.php/vl/article/view/5279) - Nils Søvik
  + [Handwriting Ergonomics](https://journals.uc.edu/index.php/vl/article/view/5280) - Henry S.R. Kao
- [1979 - Vol. 13 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/346)
  + [The Design of a Typeface](https://journals.uc.edu/index.php/vl/article/view/5266) - Gerard Unger
  + [An Experimental Approach to the Typographic Design of Textbooks](https://journals.uc.edu/index.php/vl/article/view/5267) - Dirk Wendt
  + [A Monastic Dilemma Posed by the Invention of Printing](https://journals.uc.edu/index.php/vl/article/view/5268) - Noel L. Brann
  + [Mediated Reading of the Partially Sighted](https://journals.uc.edu/index.php/vl/article/view/5269) - Hans Marmolin, Lars-Göran Nilsson, Hans Smedshammar
  + [A Different Look at Typography](https://journals.uc.edu/index.php/vl/article/view/5270) - Jean Larcher
- [1979 - Vol. 13 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/345)
  + [Let Children Show Us How to Help Them Write](https://journals.uc.edu/index.php/vl/article/view/5262) - Donald H. Graves
  + [A Dynamic Approach to Teaching Handwriting Skills](https://journals.uc.edu/index.php/vl/article/view/5263) - Iain Macleod, Peter Procter
  + [Towards a New Handwriting Adapted to the Ballpoint Pen](https://journals.uc.edu/index.php/vl/article/view/5264) - Nicolete Gray
  + [Cannons of Renaissance Handwriting](https://journals.uc.edu/index.php/vl/article/view/5265) - A.S. Osley
- [1978 - Vol. 12 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/344)
  + [Unconscious Reading: Experiments on People Who Do Not Know They Are Reading](https://journals.uc.edu/index.php/vl/article/view/5258) - Tony Marcel
  + [Toward a Visual Stylistics: Assent and Denial in Chaucer](https://journals.uc.edu/index.php/vl/article/view/5259) - Spencer Cosmos
  + [Word Recognition Reconsidered: Toward a Multi-Context Model](https://journals.uc.edu/index.php/vl/article/view/5260) - Peter Mosenthal, Sean Walmsley, Richard Allington
  + [Graphical Context of Printed Characters](https://journals.uc.edu/index.php/vl/article/view/5261) - M. Eden, C.H. Cox III, B.A. Blesser
- [1978 - Vol. 12 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/343)
  + [Value and Volume of Literacy](https://journals.uc.edu/index.php/vl/article/view/5252) - John Bormuth
  + [Spatial Aspects of Graphological Expression](https://journals.uc.edu/index.php/vl/article/view/5253) - Rudolf Arnheim
  + [Boniface: Archbishop, Legate, and Postmaster General](https://journals.uc.edu/index.php/vl/article/view/5254) - Frederick J. Cowie
  + [Effects of Line Length and Paragraph Denotation on Retrieval of Information](https://journals.uc.edu/index.php/vl/article/view/5255) - James Hartley, Peter Burnhill, Lindsey Davis
  + [Vowel Cluster Pronunciation Preferences](https://journals.uc.edu/index.php/vl/article/view/5256) - Frederick Duffelmeyer
  + [Locating Legibility Research: A Guide for the Graphic Designer](https://journals.uc.edu/index.php/vl/article/view/5257) - Jeremy Foster
- [1978 - Vol. 12 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/342)
  + [A Stage Model of Reading and Listening](https://journals.uc.edu/index.php/vl/article/view/5246) - Dominic W. Massaro
  + [Possible Components of Parallel Human Communications Systems](https://journals.uc.edu/index.php/vl/article/view/5247) - Richard E. Pastore
  + [From Print to Meaning and from Print to Sound](https://journals.uc.edu/index.php/vl/article/view/5248) - Uta Frith
  + [Using Spelling-Sound Correspondences](https://journals.uc.edu/index.php/vl/article/view/5249) - Jonathan Baron, June Hodge
  + [Dynamic Text Display for Learning to Read a Second Language](https://journals.uc.edu/index.php/vl/article/view/5250) - James G. Martin, Richard H. Meltzer, Carol B. Mills
  + [Speech Analysis During Sentence Processing: Reading and Listening](https://journals.uc.edu/index.php/vl/article/view/5251) - Betty Ann Levy
- [1977 - Vol. 11 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/341)
  + [Excerpt: Empire of Signs](https://journals.uc.edu/index.php/vl/article/view/5240) - Roland Barthes
  + [Nothing But Language: on Barthes’s Empire of Signs](https://journals.uc.edu/index.php/vl/article/view/5241) - Jay Caplan
  + [Barthes’s Excès: The Silent Apostrophe of S/Z](https://journals.uc.edu/index.php/vl/article/view/5242) - Tom Conley
  + ["B S"](https://journals.uc.edu/index.php/vl/article/view/5243) - Jane Gallop
  + [Fragments of An Amorous Discourse: Canon in Ubis](https://journals.uc.edu/index.php/vl/article/view/5244) - Randolph Runyon
  + [From Writing to the Letter: Barthes and Alphabetese](https://journals.uc.edu/index.php/vl/article/view/5245) - Steven Ungar
- [1977 - Vol. 11 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/340)
  + [Paul Claudel and Guillaume Apollinaire as Visual Poets](https://journals.uc.edu/index.php/vl/article/view/5235) - Nina S. Hellerstein
  + [A Child Learns the Alphabet](https://journals.uc.edu/index.php/vl/article/view/5236) - Lenore McCarthy
  + [Effects of Grapheme Substitutions in Text](https://journals.uc.edu/index.php/vl/article/view/5237) - Richard L. Allington, Michael Strange
  + [Calligraphy of Arnold Bank](https://journals.uc.edu/index.php/vl/article/view/5238) - Arnold Bank
  + [The Reinvention of Reading](https://journals.uc.edu/index.php/vl/article/view/5239) - Ana Hatherly
- [1977 - Vol. 11 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/339)
  + [At the Edge of Meaning](https://journals.uc.edu/index.php/vl/article/view/5222) - Aaron Marcus
  + [Observations Concerning Practical Visual Languages](https://journals.uc.edu/index.php/vl/article/view/5223) - Herbert W. Franke
  + [To the Sincere Reader](https://journals.uc.edu/index.php/vl/article/view/5224) - Nelson Howe
  + [Documentracings](https://journals.uc.edu/index.php/vl/article/view/5225) - Thomas Ockerse
  + [Computer-Produced Grey Scales](https://journals.uc.edu/index.php/vl/article/view/5226) - Ken Knowlton, Leon Harmon
  + [On Numbers, a Series of Numerical Visual Poems](https://journals.uc.edu/index.php/vl/article/view/5227) - Richard Kostelanetz
  + [Signs](https://journals.uc.edu/index.php/vl/article/view/5228) - Jonathan Price, Joel Katz
  + ["Sentence Structures" and "A Structure"](https://journals.uc.edu/index.php/vl/article/view/5229) - Robert Cumming
  + ["Sundial"](https://journals.uc.edu/index.php/vl/article/view/5230) - Ian Hamilton Finlay
  + [The Public Word](https://journals.uc.edu/index.php/vl/article/view/5231) - Alison Sky
  + [Reflections on the Theme: At the Edge of Meaning](https://journals.uc.edu/index.php/vl/article/view/5232) - Fernand Baudin
  + [A Study in Basic Design and Meaning](https://journals.uc.edu/index.php/vl/article/view/5233) - Daniel Friedman
  + [Wortgebilde Durch Spiel und Kombinatorik : Or, Why Duchamp Loved Words](https://journals.uc.edu/index.php/vl/article/view/5234) - R. C. Kenedy
- [1977 - Vol. 11 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/338)
  + [The Scientific Study of Subject Literatures](https://journals.uc.edu/index.php/vl/article/view/5216) - Conrad Rawski
  + [Cyrillic Gothic: Formal Modifications in the Design of a Russian Sans-serif Typeface](https://journals.uc.edu/index.php/vl/article/view/5217) - André Gürtler, Christian Mengelt
  + [Comprehension ofWriting and Spontaneous Speech](https://journals.uc.edu/index.php/vl/article/view/5218) - Laurence Walker
  + [Setting New Word Records](https://journals.uc.edu/index.php/vl/article/view/5219) - Randolph Quirk
  + [China's War of Words](https://journals.uc.edu/index.php/vl/article/view/5220) - David Bonavia
  + [Letters, Art, and Children](https://journals.uc.edu/index.php/vl/article/view/5221) - Carol Walklin
- [1976 - Vol. 10 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/337)
  + [Digraphia in Advertising: The Public as Guinea Pig](https://journals.uc.edu/index.php/vl/article/view/5211) - James Jaquith
  + [Internalization of English Orthographic Patterns](https://journals.uc.edu/index.php/vl/article/view/5212) - Robert H. Secrist
  + [The Phonological Relevance of Spelling Pronunciation](https://journals.uc.edu/index.php/vl/article/view/5213) - Andrew Kerek
  + [Methods and Theories of Learning to Spell Tested by Studies of Deaf Children](https://journals.uc.edu/index.php/vl/article/view/5214) - Arthur I. Gates, Esther H. Chase
  + [Notes on the History of English Spelling](https://journals.uc.edu/index.php/vl/article/view/5215) - Richard L. Venezky
- [1976 - Vol. 10 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/336)
  + [An "Ikon of the Soul": The Byzantine Letter](https://journals.uc.edu/index.php/vl/article/view/5204) - A.R. Littlewood
  + [Poetry as a Means for the Structuring of a Social Environment](https://journals.uc.edu/index.php/vl/article/view/5205) - Eugen Gomringer
  + [Statements 76](https://journals.uc.edu/index.php/vl/article/view/5206) - Jack Weiss
  + [One Hundred Essential Sight Words](https://journals.uc.edu/index.php/vl/article/view/5207) - Wayne Otto, Cathy Stallard
  + [Declaration of Independence Kit](https://journals.uc.edu/index.php/vl/article/view/5208) - Stephen G. Perrin
  + [Models for Predicting How Adults Pronounce Vowel Digraph Spellings in Unfamiliar Words](https://journals.uc.edu/index.php/vl/article/view/5209) - Richard L. Venezky, D. D. Johnson
  + ["Correspondences"](https://journals.uc.edu/index.php/vl/article/view/5210) - Jane Greengold, Chris Tanz
- [1976 - Vol. 10 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/335)
  + [Reading Readiness](https://journals.uc.edu/index.php/vl/article/view/5199) - Stanley F. Wanat
  + [Judgment of Meaningfulness of Chinese Characters by English-Speaking Observers](https://journals.uc.edu/index.php/vl/article/view/5200) - T.M. Nelson, C.J. Ladan
  + [Legibility of Numerals Displayed in a 4 x 7 Dot Matrix and Seven-Segment Digits](https://journals.uc.edu/index.php/vl/article/view/5201) - Dirk Wendt, Hans Weckerle, Bernard Orth
  + [Palindromes, Poems and Geometric Form](https://journals.uc.edu/index.php/vl/article/view/5202) - Jerrald Ranta
  + [A Course in Poetry and Printing](https://journals.uc.edu/index.php/vl/article/view/5203) - Aaron Marcus, Joe Rothrock
- [1976 - Vol. 10 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/334)
  + [A Manifesto for Visible Language](https://journals.uc.edu/index.php/vl/article/view/5196) - Merald E. Wrolstad
  + [Nomenclature of the Letterforms of Roman Type](https://journals.uc.edu/index.php/vl/article/view/5197) - Phillip Gaskell
  + [The Argument for a Semiotic Approach to Shape Writing: The Case of Italian Futurist Typography](https://journals.uc.edu/index.php/vl/article/view/5198) - John J. White
- [1975 - Vol. 9 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/333)
  + [What Is the Proper Characterization of the Alphabet?](https://journals.uc.edu/index.php/vl/article/view/5193) - W.C. Watt
  + [The Quipu as a Visible Language](https://journals.uc.edu/index.php/vl/article/view/5194) - Marcia Ascher, Robert Ascher
  + [An Orthographic Way of Writing English Prosody](https://journals.uc.edu/index.php/vl/article/view/5195) - Ernest M. Robson
- [1975 - Vol. 9 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/332)
  + [Reading Before Speaking](https://journals.uc.edu/index.php/vl/article/view/5188) - Danny D. Steinberg, Miho T. Steinberg
  + [Acquisition of Writing Skills](https://journals.uc.edu/index.php/vl/article/view/5189) - Roy A. Moxley, Jr.
  + [Line Transmitter Installation - A Poem in the Environment](https://journals.uc.edu/index.php/vl/article/view/5190) - Mark Mendel
  + [Methods of Research in Renaissance Manuscripts](https://journals.uc.edu/index.php/vl/article/view/5191) - Paul Oskar Kristeller
  + [The Medial Aspect of Language: A Linguistic Framework for Literacy](https://journals.uc.edu/index.php/vl/article/view/5192) - John Mountford
- [1975 - Vol. 9 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/331)
  + [Verbal Shape in the Poetry of Villon and Marot](https://journals.uc.edu/index.php/vl/article/view/5182) - Tom Conley
  + [The Inscription of the Whetstone from Strøm](https://journals.uc.edu/index.php/vl/article/view/5183) - Elmer H. Antonsen
  + [Letters with Alternative Basic Shapes](https://journals.uc.edu/index.php/vl/article/view/5184) - Earl M. Herrick
  + [Handwriting Education -- A Bibliography of Contemporary Publications](https://journals.uc.edu/index.php/vl/article/view/5185) - Ching Y. Suen
  + [Maurice Roche: Crâne, Carne](https://journals.uc.edu/index.php/vl/article/view/5186) - Thomas D. O'Donnell
  + [Development of Passenger/Pedestrian Oriented Symbols for Use in Transportation-Related Facilities](https://journals.uc.edu/index.php/vl/article/view/5187) - American Institute of Graphic Arts
- [1975 - Vol. 9 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/330)
  + [The Curious Role of Letter Names in Reading Instruction](https://journals.uc.edu/index.php/vl/article/view/5175) - Richard L. Venezky
  + [Radial Design in Wallace Stevens](https://journals.uc.edu/index.php/vl/article/view/5176) - Terrance J. King
  + [The Collages of William Dole](https://journals.uc.edu/index.php/vl/article/view/5177) - William Dole, Gerald Nordland
  + [The Photographic Restoration of Letterforms](https://journals.uc.edu/index.php/vl/article/view/5178) - Robert A. Hauser
  + [Research in Brief: Shapes as Cues to Word Recognition](https://journals.uc.edu/index.php/vl/article/view/5179) - Patrick Groff
  + [Excerpt: Simplifying the ABC's](https://journals.uc.edu/index.php/vl/article/view/5180) - Jay Doblin
  + [Comment: The Designer and Language](https://journals.uc.edu/index.php/vl/article/view/5181) - Alexander Nesbitt
- [1974 - Vol. 8 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/329)
  + [Records, Writing, and Decipherment](https://journals.uc.edu/index.php/vl/article/view/5171) - I. J. Gelb
  + [Visual Syntax of Concrete Poetry](https://journals.uc.edu/index.php/vl/article/view/5172) - Aaron  Marcus
  + [Four Surrealist Images](https://journals.uc.edu/index.php/vl/article/view/5173) - Edward Germain
  + [Aspects of Graffiti](https://journals.uc.edu/index.php/vl/article/view/5174) - Robert   Kostka
- [1974 - Vol. 8 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/328)
  + [Concrete Poetry: A Study in Metaphor](https://journals.uc.edu/index.php/vl/article/view/5164) - Abbie W. Beiman
  + [The Renaissance of Books](https://journals.uc.edu/index.php/vl/article/view/5165) - Northrop Frye
  + [Word Recognition Latencies as a Function of Form Class, Stem Length, and Affix Length](https://journals.uc.edu/index.php/vl/article/view/5166) - P. David Pearson, Michael L. Kamil
  + [Lettering and Society](https://journals.uc.edu/index.php/vl/article/view/5167) - Nicolete Gray
  + [A Proposition for Education in Letterforms and Handwriting](https://journals.uc.edu/index.php/vl/article/view/5168) - Wim Crouwel
  + [Printed Intonation Cues and Reading in Children](https://journals.uc.edu/index.php/vl/article/view/5169) - Linnea C. Ehri, Lee S. Wilce
  + [Orientation to the Spatial Characteristics of the Open Book](https://journals.uc.edu/index.php/vl/article/view/5170) - Marie M. Clay
- [1974 - Vol. 8 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/327)
  + [Phonological and Orthographic Relationships of Reading Performance](https://journals.uc.edu/index.php/vl/article/view/5159) - Robert A. Barganz
  + [Literacy Policy and the Emerging Technology of Readability](https://journals.uc.edu/index.php/vl/article/view/5160) - John R. Bormuth
  + [Inscriptions of Our Past](https://journals.uc.edu/index.php/vl/article/view/5161) - Francis Duval, Ivan Rigby
  + [A Bibliography in Character Recognition: Techniques for Describing Characters](https://journals.uc.edu/index.php/vl/article/view/5162) - B. Blesser, R. Shillman, C. Cox, J. Ventura, M. Eden, T. Kuklinski
  + [The Humanist in the Computer Lab](https://journals.uc.edu/index.php/vl/article/view/5163) - Joseph Raben
- [1974 - Vol. 8 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/326)
  + [A Taxonomy of Alphabets and Scripts](https://journals.uc.edu/index.php/vl/article/view/5157) - Earl M. Herrick
  + [The Rune Stones of Spirit Pond, Maine](https://journals.uc.edu/index.php/vl/article/view/5158) - Einar Haugen
- [1973 - Vol. 7 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/325)
  + [Vision, Sign, and Inference](https://journals.uc.edu/index.php/vl/article/view/5152) - William E. Hoffman
  + [Space Craft](https://journals.uc.edu/index.php/vl/article/view/5153) - David Kindersley
  + [Handwriting Legibility: A Method of Objective Evaluation](https://journals.uc.edu/index.php/vl/article/view/5154) - C.L. Lehman
  + [The Prophetess Deborah and the Invention of Printing](https://journals.uc.edu/index.php/vl/article/view/5155) - Michael Pollack
  + [Research in Brief: Understanding Tabular Display](https://journals.uc.edu/index.php/vl/article/view/5156) - Patricia Wright
- [1973 - Vol. 7 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/324)
  + [Typographs](https://journals.uc.edu/index.php/vl/article/view/5145) - Frank Harary
  + [Character Recognition Based on Phenomenological Attributes](https://journals.uc.edu/index.php/vl/article/view/5146) - B. Blesser, R. Shillman, C. Cox, T. Kuklinsky, J. Ventura, M. Eden
  + [Reading Between and Beyond the Lines](https://journals.uc.edu/index.php/vl/article/view/5147) - Malcolm P. Douglass
  + [Gothic Letterforms and Codex Vindobonensis](https://journals.uc.edu/index.php/vl/article/view/5148) - L. Allen Viehmeyer
  + [The Cue Summation Theory Tested with Meaningful Verbal Information](https://journals.uc.edu/index.php/vl/article/view/5149) - Donald R. Cushman
  + [Excerpt: Learning from Las Vegas](https://journals.uc.edu/index.php/vl/article/view/5150) - Robert Venturi, Denise Scott Brown, Steven Izenour
  + [Ode to Typography](https://journals.uc.edu/index.php/vl/article/view/5151) - Pablo Neruda
- [1973 - Vol. 7 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/323)
  + [An Explicit Formulation of the Relationship Between Tool-Using, Tool-Making, and Emergence of Langua](https://journals.uc.edu/index.php/vl/article/view/5138) - Gordon W. Hewes
  + [Formal Economy of Written Signs](https://journals.uc.edu/index.php/vl/article/view/5139) - E.J.W. Barber
  + [Is Literacy Acquisition Easier in Some Languages Than in Others?](https://journals.uc.edu/index.php/vl/article/view/5140) - John Downing
  + [Introductory Education in Typography](https://journals.uc.edu/index.php/vl/article/view/5141) - Daniel Friedman
  + [Archives in the Ancient World](https://journals.uc.edu/index.php/vl/article/view/5142) - Ernest Posner
  + [Subjective Preference and Retrieval of Information from Reference Materials](https://journals.uc.edu/index.php/vl/article/view/5143) - James Hartley, Peter Burnhill, Susan J. Timson
  + [The Future Role of the Printing Historical Society](https://journals.uc.edu/index.php/vl/article/view/5144) - James Moran
- [1973 - Vol. 7 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/322)
  + [Greek Letterforms on the Parthian Drachms](https://journals.uc.edu/index.php/vl/article/view/5134) - Richard A. Olson
  + [The Language of Capitalization in Shakespeare's First Folio](https://journals.uc.edu/index.php/vl/article/view/5135) - Carleton S. Tritt
  + [Visible Language: An Experimental Course](https://journals.uc.edu/index.php/vl/article/view/5136) - Sharon H. Poggenpohl
  + [Discrimination of Three Types of Graphic Stimuli](https://journals.uc.edu/index.php/vl/article/view/5137) - Henry G. Timko
- [1972 - Vol. 6 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/321)
  + [One Second of Reading](https://journals.uc.edu/index.php/vl/article/view/5129) - Philip B. Gough
  + [The Typographic Element in Cubism, 1911-1915](https://journals.uc.edu/index.php/vl/article/view/5130) - Susan Marcu
  + [Changing Responsibilities of the Typographic Designer](https://journals.uc.edu/index.php/vl/article/view/5131) - G.W. Ovink
  + [Program for Developing Visual Symbols](https://journals.uc.edu/index.php/vl/article/view/5132) - Ed Bedno
  + [Words about Ed Ruscha](https://journals.uc.edu/index.php/vl/article/view/5133) - David Bourdon
- [1972 - Vol. 6 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/320)
  + [After the Book?](https://journals.uc.edu/index.php/vl/article/view/5125) - George Steiner
  + [Four Replies to George Steiner's Address to the Ferguson Seminar in Publishing](https://journals.uc.edu/index.php/vl/article/view/5126) - John V. Brain, Norman S. Fiering, John Freeman, Leland E. Warren
  + [From Mistress to Master: The Origins of Polyphonic Music and as a Visible Language](https://journals.uc.edu/index.php/vl/article/view/5127) - Gordon K. Greene
  + [Visual Language in the Old English Cadmonian Genesis](https://journals.uc.edu/index.php/vl/article/view/5128) - Thomas H. Ohlgren
- [1972 - Vol. 6 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/319)
  + [Typography and the Visual Concrete Poem](https://journals.uc.edu/index.php/vl/article/view/5120) - Mary Ellen Solt
  + [Proposal for a Diagrammatic Language for Design](https://journals.uc.edu/index.php/vl/article/view/5121) - Robert E. David
  + [Orthographic Practices of Elias Molee](https://journals.uc.edu/index.php/vl/article/view/5122) - Henry R. Stern
  + [Grapheme-Phoneme Correspondence in Beginning Reading of Disadvantaged Five-Year-Olds](https://journals.uc.edu/index.php/vl/article/view/5123) - Myrtle Scott
  + [On Effects of Indentation and Underlining in Reference Work](https://journals.uc.edu/index.php/vl/article/view/5124) - Dirk Wendt, Hans Weckerle
- [1972 - Vol. 6 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/318)
  + [Origin of the Sexagesimal System: Interaction of Language and Writing](https://journals.uc.edu/index.php/vl/article/view/5115) - Marvin A. Powell, Jr.
  + [Development of Visual Poetry in France](https://journals.uc.edu/index.php/vl/article/view/5116) - David W. Seaman
  + [Development of Word Perception and Problem Solving Strategies](https://journals.uc.edu/index.php/vl/article/view/5117) - Lita Furby
  + [The Work of Bruce Rogers](https://journals.uc.edu/index.php/vl/article/view/5118) - James Wells
  + [Homage to Alberto Tallone, 1898-1960](https://journals.uc.edu/index.php/vl/article/view/5119) - Jack W. Stauffacher
- [1971 - Vol. 5 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/317)
  + [Ancient Maya Writing and Calligraphy](https://journals.uc.edu/index.php/vl/article/view/5110) - Michael D. Coe
  + [Design of a New Japanese Typeface: Typos](https://journals.uc.edu/index.php/vl/article/view/5111) - -- Group Typo
  + [Letterforms as a Medium for Artistic Expression](https://journals.uc.edu/index.php/vl/article/view/5112) - Hella Basu
  + [Cresci and His Capital Alphabet](https://journals.uc.edu/index.php/vl/article/view/5113) - Donald M. Anderson
  + [Why Serifs are Important: the Perception of Small Print](https://journals.uc.edu/index.php/vl/article/view/5114) - David Owen Robinson, Michael Abbamonte, H. Selby
- [1971 - Vol. 5 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/316)
  + [A Prototype Computerized Page-design System](https://journals.uc.edu/index.php/vl/article/view/5105) - Aaron Marcus
  + [Letter Names of the Latin Alphabet](https://journals.uc.edu/index.php/vl/article/view/5106) - Arthur E. Gordon
  + [Steps toward Handwriting Analysis and Recognition](https://journals.uc.edu/index.php/vl/article/view/5107) - Makoto Yasuhara
  + [Calligraphy of Ch'an and Zen Monks](https://journals.uc.edu/index.php/vl/article/view/5108) - Jan Fontein, Money L. Hickman
  + [Experiments with Unjustified Text](https://journals.uc.edu/index.php/vl/article/view/5109) - Peter Burnhill, James Harley
- [1971 - Vol. 5 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/315)
  + [Linguistic Features of Scribal Spacing](https://journals.uc.edu/index.php/vl/article/view/5100) - Virginia J. Cyrus
  + [Use of the Hyphen to Indicate Divided Words](https://journals.uc.edu/index.php/vl/article/view/5101) - F.M. O'Hara, Jr.
  + [Automated Reading of the Printed Page](https://journals.uc.edu/index.php/vl/article/view/5102) - Donald E. Troxel
  + [Vocalism in Silent Reading](https://journals.uc.edu/index.php/vl/article/view/5103) - Donald L. Cleland
  + [Durability of Fifteenth-century Type](https://journals.uc.edu/index.php/vl/article/view/5104) - Michael Pollak
- [1971 - Vol. 5 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/314)
  + [Calligraphy – An Aid to Cartography?](https://journals.uc.edu/index.php/vl/article/view/5096) - A.S. Osley
  + [The Development of Vidifont](https://journals.uc.edu/index.php/vl/article/view/5097) - Rudi Bass
  + [Creating A Munduruku Orthography](https://journals.uc.edu/index.php/vl/article/view/5098) - Marjorie Crofts
  + [Type Design Classification](https://journals.uc.edu/index.php/vl/article/view/5099) - Walter Tracy
- [1970 - Vol. 4 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/313)
  + [Some Psycholinguistic Components of Initial Standard Literacy](https://journals.uc.edu/index.php/vl/article/view/5090) - John Mountford
  + [The Emergence of Gothic Handwriting](https://journals.uc.edu/index.php/vl/article/view/5091) - Leonard E. Boyle
  + [Some Thoughts of the Use and Disuse of Diacritics](https://journals.uc.edu/index.php/vl/article/view/5092) - Louis Marck
  + [Letterforms in Photo-typography](https://journals.uc.edu/index.php/vl/article/view/5093) - Adrian Frutiger
  + [An Index of the Quality of a Hyphenation Algorithm](https://journals.uc.edu/index.php/vl/article/view/5094) - Lindsay Molyneux
  + [Typographic Education: Headings in Text](https://journals.uc.edu/index.php/vl/article/view/5095) - Peter Burnhill
- [1970 - Vol. 4 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/312)
  + [Words in Their Place](https://journals.uc.edu/index.php/vl/article/view/5086) - Rudolf Arnheim
  + [Broken Scripts and the Classification of Typefaces](https://journals.uc.edu/index.php/vl/article/view/5087) - Gerrit Noordzij
  + [The Siloam Inscription and Alphabetic Origins](https://journals.uc.edu/index.php/vl/article/view/5088) - Roy K. Patteson, Jr.
  + [Times Roman: A Re-assessment](https://journals.uc.edu/index.php/vl/article/view/5089) - Allen Hunt
- [1970 - Vol. 4 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/308)
  + [Psycholinguistic Universals in the Reading Process](https://journals.uc.edu/index.php/vl/article/view/5067) - Kenneth S. Goodman
  + [The Genesis of the Russian Grazhdanskii Shrift or Civil Type—Part II](https://journals.uc.edu/index.php/vl/article/view/5068) - Ivan L. Kaldor
  + [Directional Consistency in Form Identification](https://journals.uc.edu/index.php/vl/article/view/5069) - Jeremy J. Foster
  + [Experimental Use of the Search Task in an Analysis of Type Legibility in Cartography](https://journals.uc.edu/index.php/vl/article/view/5070) - Barbara S. Bartz
- [1970 - Vol. 4 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/307)
  + [An Investigation of the Design and Performance of Traffic Control Devices](https://journals.uc.edu/index.php/vl/article/view/5059) - John Lees, Melvin Farman
  + [Ligature Design for Contemporary Technology](https://journals.uc.edu/index.php/vl/article/view/5060) - Joseph S. Scorsone
  + [Type Design for the Computer Age](https://journals.uc.edu/index.php/vl/article/view/5061) - Wim Crouwel
  + [Reader Preferences for Typeface and Leading](https://journals.uc.edu/index.php/vl/article/view/5062) - D. Becker, J. Heinrich, R. von Sichowsky, D. Wendt
  + [Designing the Initial Teaching Alphabet in Five Typeface](https://journals.uc.edu/index.php/vl/article/view/5063) - Arleigh Montague
  + [Speed-reading Made Easy](https://journals.uc.edu/index.php/vl/article/view/5064) - W. S. Brown
  + [Comment: Voice, Print, and Culture](https://journals.uc.edu/index.php/vl/article/view/5065) - Walter J. Ong
  + [Reading the Journal](https://journals.uc.edu/index.php/vl/article/view/5066) - Gerrit Noordzij
- [1969 - Vol. 3 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/311)
  + [The Genesis of Russian Grazhdanskii Shrift or Civil Type—Part I](https://journals.uc.edu/index.php/vl/article/view/5081) - Ivan L. Kaldor
  + [Visual Language from the Verbal Model](https://journals.uc.edu/index.php/vl/article/view/5082) - Colin Murray Turbayne
  + [Fashion in Type Design](https://journals.uc.edu/index.php/vl/article/view/5083) - G.W. Ovink
  + [The House-style of The Netherlands PTT](https://journals.uc.edu/index.php/vl/article/view/5084) - Pieter Brattinga
  + [Search: An Approach to Cartographic Type Legibility Measurement](https://journals.uc.edu/index.php/vl/article/view/5085) - Barbara S. Bartz
- [1969 - Vol. 3 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/305)
  + [Mallarmé: The Transcendence of Language and the Aesthetics of the Book](https://journals.uc.edu/index.php/vl/article/view/5049) - Gerald L. Bruns
  + [O or 0?](https://journals.uc.edu/index.php/vl/article/view/5050) - Dirk Wendt
  + [A Proposed Fontstyle for the Graphic Representation of the Oh and Zero](https://journals.uc.edu/index.php/vl/article/view/5051) - Allen G. Vartabedian
  + [The Use of Type Damage as Evidence in Bibliographical Description](https://journals.uc.edu/index.php/vl/article/view/5052) - G. Thomas Tanselle
  + [A Report Generator Approach to Automated Page Composition](https://journals.uc.edu/index.php/vl/article/view/5053) - J.R. Burns
- [1969 - Vol. 3 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/304)
  + [Letterform Research Needs Definition and Direction](https://journals.uc.edu/index.php/vl/article/view/5043) - Merald E. Wrolstad
  + [Type Variation and the Problem of Cartographic Type Legibility](https://journals.uc.edu/index.php/vl/article/view/5044) - Barbara S. Bartz
  + [Clues to a Letter's Recognition: Implications for the Design of Characters](https://journals.uc.edu/index.php/vl/article/view/5045) - Paul A. Kolers
  + [Visual-motor Skills: Response Characteristics and Prereading Behavior](https://journals.uc.edu/index.php/vl/article/view/5046) - Katherine P. DiMeo
  + [A Standard Code for Typographic Character Identification](https://journals.uc.edu/index.php/vl/article/view/5047) - Stanley Rice
  + [Excerpt: Typography That Makes the Reader Work](https://journals.uc.edu/index.php/vl/article/view/5048) - Joel A. Roth
- [1969 - Vol. 3 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/303)
  + [The First Alphabetical Treatises in the Renaissance](https://journals.uc.edu/index.php/vl/article/view/5039) - Millard Meiss
  + [Computer Recognition of Hand-printed Text](https://journals.uc.edu/index.php/vl/article/view/5040) - John H. Munson
  + [Recognizing the Marks on Paper](https://journals.uc.edu/index.php/vl/article/view/5041) - R.M.N. Crosby
  + [Relative Legibility of Leroy and Lincoln/MITRE Fonts on Television](https://journals.uc.edu/index.php/vl/article/view/5042) - David Shurtleff
- [1968 - Vol. 2 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/302)
  + [One Approach to Computer Assisted Letter Design](https://journals.uc.edu/index.php/vl/article/view/5032) - H. W. Mergler, P. M. Vargo
  + [The Typography of El Lissitzky](https://journals.uc.edu/index.php/vl/article/view/5033) - M. V. Mathews, L. Leering-van Moorsel
  + [Esthetic Values in Computerized Photocomposition](https://journals.uc.edu/index.php/vl/article/view/5034) - John W. Seybold
  + [Changes in Letterforms Due to Technical Developments](https://journals.uc.edu/index.php/vl/article/view/5035) - Hermann Zapf
  + [Context Clues as an Aid to the Reader](https://journals.uc.edu/index.php/vl/article/view/5036) - Robert Emans
  + [Commentary : Linguistics, "Writing," and Typography](https://journals.uc.edu/index.php/vl/article/view/5037) - Ernest Hoch
  + [Book Reviews](https://journals.uc.edu/index.php/vl/article/view/5038) - Henry Hawley, Peter Mayer
- [1968 - Vol. 2 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/301)
  + [Quantifying Order in Typographic Design](https://journals.uc.edu/index.php/vl/article/view/5026) - Gui Bonsiepe
  + ["Writing" and "Alphabet"](https://journals.uc.edu/index.php/vl/article/view/5027) - John Mountford
  + [Standardization of Alphabetic Graphemes](https://journals.uc.edu/index.php/vl/article/view/5028) - S.B. Telingater
  + [Typographic Anamorphosis](https://journals.uc.edu/index.php/vl/article/view/5029) - Jan Slothouber, William Graatsma
  + [Towards a Standard for Measuring the Accuracy of Any Computer-hyphenation Program](https://journals.uc.edu/index.php/vl/article/view/5030) - Dwight D. Brown
  + [Pictures to be Read/Poetry to be Seen](https://journals.uc.edu/index.php/vl/article/view/5031) - Jan van der Marck
- [1968 - Vol. 2 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/300)
  + [Type Reading Machines for the Blind](https://journals.uc.edu/index.php/vl/article/view/5021) - Glendon C. Smith
  + [Reader Response to Newspaper Front-page Format](https://journals.uc.edu/index.php/vl/article/view/5022) - J.W. Click, G.H. Stempel
  + [Words Fragments as Aids to Recall: the Organization of a Word](https://journals.uc.edu/index.php/vl/article/view/5023) - L.M. Horowitz, M.A. White, D.W. Atwood
  + [Readership of Advertisements with All-display Typography](https://journals.uc.edu/index.php/vl/article/view/5024) - A.T. Turnbull, D.E. Carter
  + [The Diagram Is the Message](https://journals.uc.edu/index.php/vl/article/view/5025) - Jesse H. Shera, Conrad H. Rawski
- [1968 - Vol. 2 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/299)
  + [Semantic Differentials of Typefaces as a Method of Congeniality Research](https://journals.uc.edu/index.php/vl/article/view/5016) - Dirk Wendt
  + [The Design of Egyptian 505](https://journals.uc.edu/index.php/vl/article/view/5017) - André Gürtler
  + [Words in Color: Two Experimental Studies](https://journals.uc.edu/index.php/vl/article/view/5018) - Lillian R. Hinds
  + [Research Report on Color Story Reading](https://journals.uc.edu/index.php/vl/article/view/5019) - F. Kenneth Jones
  + [Adjustment to Unjustified Composition on the Rotterdamsch Nieuwsblad](https://journals.uc.edu/index.php/vl/article/view/5020) - C.H. Evers
- [1967 - Vol. 1 - No. 4](https://journals.uc.edu/index.php/vl/issue/view/298)
- [1967 - Vol. 1 - No. 3](https://journals.uc.edu/index.php/vl/issue/view/297)
  + [Pictographs, Ideograms, and Alphabets in the Work of Paul Klee](https://journals.uc.edu/index.php/vl/article/view/4999) - James Smith Pierce
  + [A Chronological List of Type-setting Machines and Ancillary Equipment, 1822-1925](https://journals.uc.edu/index.php/vl/article/view/5000) - Richard E. Huss
  + [Studies of the Efficiency of Drug Labelling](https://journals.uc.edu/index.php/vl/article/view/5001) - M. Hailstone, J. J. Foster
  + [Typography with the IBM Selectric Composer](https://journals.uc.edu/index.php/vl/article/view/5002) - Adrian Frutiger
  + [Concrete Poetry](https://journals.uc.edu/index.php/vl/article/view/5003) - Mike Weaver
  + [Research in Progress](https://journals.uc.edu/index.php/vl/article/view/5004) - Dirk Wendt
  + [Exhibition Review](https://journals.uc.edu/index.php/vl/article/view/5005) - R. Hunter Middleton
  + [Book Review](https://journals.uc.edu/index.php/vl/article/view/5006) - William J. Carlton
- [1967 - Vol. 1 - No. 2](https://journals.uc.edu/index.php/vl/issue/view/296)
  + [Communication Theory and Typographic Research](https://journals.uc.edu/index.php/vl/article/view/4990) - Randall Harrison
  + [Readability of Typewritten Material: Proportional Versus Standard Spacing](https://journals.uc.edu/index.php/vl/article/view/4991) - Donald E. Payne
  + [OCR-B: A Standardized Character for Optical Recognition](https://journals.uc.edu/index.php/vl/article/view/4992) - Adrian Frutiger
  + [An Investigation of Visual Discrimination Training for Beginning Readers](https://journals.uc.edu/index.php/vl/article/view/4993) - Warren H. Wheelock, Nicholas J. Silvaroli
  + [Typographic Research and Bibliography](https://journals.uc.edu/index.php/vl/article/view/4994) - G. Thomas Tanselle
  + [Print Layout and Design with a Computer CRT System](https://journals.uc.edu/index.php/vl/article/view/4995) - R. J. Wakefield
  + [Secondary Uses of Letters in Language](https://journals.uc.edu/index.php/vl/article/view/4996) - Yakov Malkiel
  + [Commentary: Methodological Problems in Research on Simplified Alphabets and Regularized Writing-Systems](https://journals.uc.edu/index.php/vl/article/view/4997) - John Downing, Edward Fry
  + [Book Reviews](https://journals.uc.edu/index.php/vl/article/view/4998) - Fernand Baudin, Philippe Schuwer
- [1967 - Vol. 1 - No. 1](https://journals.uc.edu/index.php/vl/issue/view/295)
  + [Effects of Three Typographical Variables on Speed of Reading](https://journals.uc.edu/index.php/vl/article/view/4981) - Richard H. Wiggins
  + [The Diacritical Marketing System and a Preliminary Comparison with the Initial Teaching Alphabet](https://journals.uc.edu/index.php/vl/article/view/4982) - Edward Fry
  + [Printing for the Visually Handicapped](https://journals.uc.edu/index.php/vl/article/view/4983) - J. H. Prince
  + [Line Scan Standards for Characters and Symbols: a Practical Study](https://journals.uc.edu/index.php/vl/article/view/4984) - C. J. Duncan
  + [The Perspectives for Practical Optical Character Recognition](https://journals.uc.edu/index.php/vl/article/view/4985) - M. Nadler
  + [Typographical Effects by Cathode Ray Tube Typesetting Systems](https://journals.uc.edu/index.php/vl/article/view/4986) - F. C. Holland
  + [On-Line Visual Correction and Make-up Systems--I : Hardware](https://journals.uc.edu/index.php/vl/article/view/4987) - C. I.  Cowan
  + [Readability as a Function of the Straightness of Right-hand Margins](https://journals.uc.edu/index.php/vl/article/view/4988) - Ralph Fabrizio, Ira Kaplan, Gilbert Teal
  + [Secondary Uses of Letters in Language](https://journals.uc.edu/index.php/vl/article/view/4989) - Yakov Malkiel
